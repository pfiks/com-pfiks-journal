/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.service.impl;

import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.pfiks.journal.exception.JournalProcessingException;
import com.pfiks.journal.model.JournalArticleContent;
import com.pfiks.journal.service.JournalArticleProcessor;

@Component(immediate = true, service = JournalArticleProcessor.class)
public class JournalArticleProcessorImpl implements JournalArticleProcessor {

	private JournalArticleLocalService journalArticleLocalService;

	@Override
	public String getPopulatedContent(long groupId, String articleId, Map<String, String> values) throws JournalProcessingException {
		String[] placeholders = values.keySet().toArray(new String[values.size()]);
		String[] newValues = values.values().toArray(new String[values.size()]);
		return StringUtil.replace(getArticleContent(groupId, articleId), placeholders, newValues);
	}

	@Override
	public String getPopulatedContent(String articleContent, Map<String, String> placeholders) {
		String[] placeholderKeys = placeholders.keySet().toArray(new String[placeholders.size()]);
		String[] placeholderValues = placeholders.values().toArray(new String[placeholders.size()]);
		return StringUtil.replace(articleContent, placeholderKeys, placeholderValues);
	}

	@Override
	public String getArticleContent(long groupId, String articleId, String languageId) throws JournalProcessingException {
		try {
			JournalArticle journalArticle = journalArticleLocalService.getLatestArticle(groupId, articleId);
			return journalArticle.getContentByLocale(languageId);
		} catch (Exception e) {
			throw new JournalProcessingException("Exception retrieving article content", e);
		}
	}

	@Override
	public String getArticleContent(long groupId, String articleId) throws JournalProcessingException {
		try {
			Locale siteDefaultLocale = PortalUtil.getSiteDefaultLocale(groupId);
			return getArticleContent(groupId, articleId, LanguageUtil.getLanguageId(siteDefaultLocale));
		} catch (Exception e) {
			throw new JournalProcessingException("Exception retrieving article content", e);
		}
	}

	@Override
	public Map<String, String> getArticleContentByLanguageId(long groupId, String articleId) throws JournalProcessingException {
		try {
			return getJournalArticleContentByLanguageId(journalArticleLocalService.getLatestArticle(groupId, articleId));
		} catch (Exception e) {
			throw new JournalProcessingException("Exception retrieving article content", e);
		}
	}

	@Override
	public Map<String, String> getArticleContentByLanguageId(JournalArticle journalArticle) {
		return getJournalArticleContentByLanguageId(journalArticle);
	}

	@Override
	public String getFieldValue(String fieldName, String xmlToParse) throws JournalProcessingException {
		try {
			Reader reader = new StringReader(xmlToParse);
			Document document = SAXReaderUtil.read(reader);
			Node node = document.selectSingleNode("/root/dynamic-element[@name='" + fieldName + "']/dynamic-content");
			return node.getText();
		} catch (Exception e) {
			throw new JournalProcessingException("Exception retrieving field value", e);
		}
	}

	@Override
	public JournalArticleContent getJournalArticleContent(JournalArticle journalArticle, String languageId) throws JournalProcessingException {
		try {
			String articleContent = journalArticle.getContentByLocale(languageId);

			JAXBContext jaxbContext = JAXBContext.newInstance(JournalArticleContent.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(articleContent);
			return (JournalArticleContent) unmarshaller.unmarshal(reader);
		} catch (Exception e) {
			throw new JournalProcessingException(e);
		}
	}

	@Override
	public JournalArticleContent getJournalArticleContent(JournalArticle journalArticle) throws JournalProcessingException {
		try {
			String articleContent = journalArticle.getContent();

			JAXBContext jaxbContext = JAXBContext.newInstance(JournalArticleContent.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(articleContent);
			return (JournalArticleContent) unmarshaller.unmarshal(reader);
		} catch (Exception e) {
			throw new JournalProcessingException(e);
		}
	}

	private Map<String, String> getJournalArticleContentByLanguageId(JournalArticle journalArticle) {
		Map<String, String> results = new HashMap<>();

		String[] availableLanguageIds = journalArticle.getAvailableLanguageIds();

		String defaultLanguageId = journalArticle.getDefaultLanguageId();
		if (!ArrayUtil.contains(availableLanguageIds, defaultLanguageId)) {
			String defaultContent = journalArticle.getContentByLocale(defaultLanguageId);
			results.put(defaultLanguageId, defaultContent);
		}

		for (String languageId : availableLanguageIds) {
			String contentByLocale = journalArticle.getContentByLocale(languageId);
			results.put(languageId, contentByLocale);
		}
		return results;
	}

	@Reference(service = JournalArticleLocalService.class)
	protected void setJournalArticleLocalService(JournalArticleLocalService journalArticleLocalService) {
		this.journalArticleLocalService = journalArticleLocalService;
	}
}
