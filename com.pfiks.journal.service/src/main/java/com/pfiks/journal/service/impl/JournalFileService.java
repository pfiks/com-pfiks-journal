/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.service.impl;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;

import com.pfiks.journal.constants.WebContentStructure;

@Component(immediate = true, properties = {}, service = JournalFileService.class)
public class JournalFileService {

	public InputStream getStructureFile(WebContentStructure structure) {
		return loadFile(structure, ".json");
	}

	public InputStream getStructureTemplate(WebContentStructure structure) {
		return loadFile(structure, ".ftl");
	}

	private InputStream loadFile(WebContentStructure structure, String extension) {
		String structureName = structure.getValue();
		String path = "import/" + structureName + "/" + structureName + extension;
		return getClass().getClassLoader().getResourceAsStream(path);
	}

}
