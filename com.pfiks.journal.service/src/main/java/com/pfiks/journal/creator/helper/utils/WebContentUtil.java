/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.utils;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowThreadLocal;
import com.pfiks.common.exception.CommonUtilsException;
import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.journal.creator.helper.utils.constants.CreationConstants;

@Component(immediate = true, service = WebContentUtil.class)
public class WebContentUtil {

	private static final Log LOG = LogFactoryUtil.getLog(WebContentUtil.class);

	private DDMStructureLocalService ddmStructureLocalService;
	private DDMTemplateLocalService ddmTemplateLocalService;
	private GroupLocalService groupLocalService;
	private JournalArticleLocalService journalArticleLocalService;
	private LocaleMapUtil localeMapUtil;
	private ServiceContextHelper serviceContextHelper;
	private UserLocalService userLocalService;

	public String getDefaultTemplateKeyForStructure(DDMStructure ddmStructure) throws PortalException {
		if (Validator.isNotNull(ddmStructure)) {
			List<DDMTemplate> templates = ddmStructure.getTemplates();
			if (!templates.isEmpty()) {
				return templates.get(0).getTemplateKey();
			}
		}
		return CreationConstants.BASIC_WEB_CONTENT_TEMPLATE_KEY;
	}

	public long getJournalClassNameId() {
		return PortalUtil.getClassNameId(JournalArticle.class);
	}

	public long getGlobalGroupId(long companyId) throws PortalException {
		return groupLocalService.getCompanyGroup(companyId).getGroupId();
	}

	public ServiceContext getServiceContext(long companyId, long groupId) throws PortalException {
		long defaultUserId = userLocalService.getDefaultUserId(companyId);
		return serviceContextHelper.getServiceContext(companyId, groupId, defaultUserId);
	}

	public DDMStructure checkWebContentStructure(ServiceContext serviceContext, long globalGroupId, DDMStructure ddmStructure) throws PortalException {
		if (Validator.isNotNull(ddmStructure)) {
			return ddmStructure;
		}

		long journalClassNameId = getJournalClassNameId();
		DDMStructure basicStructure = ddmStructureLocalService.fetchStructure(globalGroupId, journalClassNameId, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY);

		if (Validator.isNull(basicStructure)) {
			LOG.info("Copying basic web content structure from default company");
			long structureClassnameId = PortalUtil.getClassNameId(DDMStructure.class);
			long defaultGlobalGroupId = getGlobalGroupId(PortalUtil.getDefaultCompanyId());
			DDMStructure defaultCompanyStructure = ddmStructureLocalService.getStructure(defaultGlobalGroupId, journalClassNameId, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY);
			DDMTemplate defaultCompanyTemplate = defaultCompanyStructure.getTemplates().get(0);

			basicStructure = ddmStructureLocalService.addStructure(serviceContext.getUserId(), globalGroupId, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, journalClassNameId,
					defaultCompanyStructure.getStructureKey(), defaultCompanyStructure.getNameMap(), defaultCompanyStructure.getDescriptionMap(), defaultCompanyStructure.getDDMForm(),
					defaultCompanyStructure.getDDMFormLayout(), defaultCompanyStructure.getStorageType(), defaultCompanyStructure.getType(), serviceContext);

			ddmTemplateLocalService.addTemplate(serviceContext.getUserId(), globalGroupId, structureClassnameId, basicStructure.getStructureId(), journalClassNameId,
					CreationConstants.BASIC_WEB_CONTENT_TEMPLATE_KEY, defaultCompanyTemplate.getNameMap(), defaultCompanyTemplate.getDescriptionMap(), defaultCompanyTemplate.getType(),
					defaultCompanyTemplate.getMode(), defaultCompanyTemplate.getLanguage(), defaultCompanyTemplate.getScript(), defaultCompanyTemplate.isCacheable(),
					defaultCompanyTemplate.getSmallImage(), defaultCompanyTemplate.getSmallImageURL(), null, serviceContext);
		}
		return basicStructure;
	}

	public JournalArticle getOrCreateJournalArticle(ServiceContext serviceContext, String articleId, String articleTitle, String content, JournalFolder journalFolder, DDMStructure ddmStructure,
			String ddmTemplateKey, boolean skipWorkflow) throws CommonUtilsException, PortalException {

		long scopeGroupId = serviceContext.getScopeGroupId();
		Optional<JournalArticle> journalArticle = getJournalArticle(scopeGroupId, articleId);
		if (journalArticle.isPresent()) {
			LOG.debug("Web content article already exists in groupId: " + scopeGroupId + " with articleId: " + articleId);
			return journalArticle.get();
		} else {

			boolean enabled = WorkflowThreadLocal.isEnabled();
			boolean updateWorkflow = false;
			if (skipWorkflow && enabled) {
				WorkflowThreadLocal.setEnabled(false);
				updateWorkflow = true;
			}
			boolean autoArticleId = Validator.isNull(articleId);
			Map<Locale, String> titleMap = localeMapUtil.getLocaleMapForAvailaleLocales(articleTitle);

			// Default values
			double version = 1.0;
			long classNameId = 0;
			long classPK = 0;
			Map<Locale, String> descriptionMap = null;
			String layoutUuid = null;
			Calendar now = Calendar.getInstance();
			int displayDateMonth = now.get(Calendar.MONTH);
			int displayDateDay = now.get(Calendar.DAY_OF_MONTH);
			int displayDateYear = now.get(Calendar.YEAR);
			int displayDateHour = 0;
			int displayDateMinute = 0;
			int expirationDateMonth = 0;
			int expirationDateDay = 0;
			int expirationDateYear = 0;
			int expirationDateHour = 0;
			int expirationDateMinute = 0;
			boolean neverExpire = true;
			int reviewDateDay = 0;
			int reviewDateYear = 0;
			int reviewDateMonth = 0;
			int reviewDateHour = 0;
			int reviewDateMinute = 0;
			boolean neverReview = true;
			boolean indexable = true;
			boolean smallImage = false;
			String smallImageURL = StringPool.BLANK;
			File smallImageFile = null;
			Map<String, byte[]> images = null;
			String articleURL = null;

			long userId = serviceContext.getUserId();
			long folderId = Validator.isNotNull(journalFolder) ? journalFolder.getFolderId() : JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID;
			String structureKey = ddmStructure.getStructureKey();

			JournalArticle article = journalArticleLocalService.addArticle(userId, scopeGroupId, folderId, classNameId, classPK, articleId, autoArticleId, version, titleMap, descriptionMap, content,
					structureKey, ddmTemplateKey, layoutUuid, displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute, expirationDateMonth, expirationDateDay,
					expirationDateYear, expirationDateHour, expirationDateMinute, neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute, neverReview, indexable,
					smallImage, smallImageURL, smallImageFile, images, articleURL, serviceContext);

			if (updateWorkflow) {
				WorkflowThreadLocal.setEnabled(enabled);
			}
			LOG.debug("Created new JournalArticle in groupId: " + scopeGroupId + ", articleId: " + articleId);
			return article;
		}
	}

	private Optional<JournalArticle> getJournalArticle(long groupId, String articleId) {
		try {
			return Optional.of(journalArticleLocalService.getLatestArticle(groupId, articleId));
		} catch (Exception e) {
			LOG.debug(e);
			return Optional.empty();
		}
	}

	@Reference
	protected void setDDMStructureLocalService(DDMStructureLocalService ddmStructureLocalService) {
		this.ddmStructureLocalService = ddmStructureLocalService;
	}

	@Reference
	protected void setDDMTemplateLocalService(DDMTemplateLocalService ddmTemplateLocalService) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Reference
	protected void setGroupLocalService(GroupLocalService groupLocalService) {
		this.groupLocalService = groupLocalService;
	}

	@Reference
	protected void setJournalArticleLocalService(JournalArticleLocalService journalArticleLocalService) {
		this.journalArticleLocalService = journalArticleLocalService;
	}

	@Reference
	protected void setLocaleMapUtil(LocaleMapUtil localeMapUtil) {
		this.localeMapUtil = localeMapUtil;
	}

	@Reference
	protected void setServiceContextHelper(ServiceContextHelper serviceContextHelper) {
		this.serviceContextHelper = serviceContextHelper;
	}

	@Reference
	protected void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

}
