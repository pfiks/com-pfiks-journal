/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.impl;

import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.journal.constants.WebContentStructure;
import com.pfiks.journal.creator.helper.DDMStructureCreatorHelper;
import com.pfiks.journal.creator.helper.JournalArticleCreatorHelper;
import com.pfiks.journal.creator.helper.JournalFolderCreatorHelper;
import com.pfiks.journal.creator.helper.utils.WebContentUtil;
import com.pfiks.journal.creator.helper.utils.constants.CreationConstants;
import com.pfiks.journal.exception.JournalCreationException;

@Component(immediate = true, property = {}, service = JournalArticleCreatorHelper.class)
public class JournalArticleCreatorHelperImpl implements JournalArticleCreatorHelper {

	private static final Log LOG = LogFactoryUtil.getLog(JournalArticleCreatorHelperImpl.class);

	private DDMStructureCreatorHelper ddmStructureCreatorHelper;
	private DDMStructureLocalService ddmStructureLocalService;
	private InputOuputUtil inputOutputUtil;
	private JournalFolderCreatorHelper journalFolderCreatorHelper;
	private WebContentUtil webContentUtil;
	private GroupLocalService groupLocalService;

	@Override
	public void createMissingJournalArticle(long companyId, long groupId, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleFileName, InputStream inputStream)
			throws JournalCreationException {
		getOrCreateJournalArticle(groupId, ddmStructure, journalFolder, skipWorkflow, articleFileName, inputStream);
	}

	@Override
	public JournalArticle getOrCreateJournalArticle(long groupId, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleFileName, InputStream inputStream)
			throws JournalCreationException {
		try {
			Group group = groupLocalService.getGroup(groupId);
			return getOrCreateJournalArticle(group, ddmStructure, journalFolder, skipWorkflow, articleFileName, inputStream);
		} catch (Exception e) {
			LOG.error("Exception creating new webcontent article", e);
			throw new JournalCreationException("Exception creating new webcontent article", e);
		}
	}

	@Override
	public void createMissingJournalArticle(Group group, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleFileName, InputStream inputStream)
			throws JournalCreationException {
		getOrCreateJournalArticle(group, ddmStructure, journalFolder, skipWorkflow, articleFileName, inputStream);
	}

	@Override
	public void createMissingJournalArticle(long companyId, long groupId, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleId, String title, String content)
			throws JournalCreationException {
		getOrCreateJournalArticle(companyId, groupId, ddmStructure, journalFolder, skipWorkflow, articleId, title, content);
	}

	@Override
	public void createMissingJournalArticles(long companyId, long groupId, String structureKey, String folderName, Map<String, InputStream> inputStreams) throws JournalCreationException {
		try {
			if (!inputStreams.isEmpty()) {
				ServiceContext serviceContext = webContentUtil.getServiceContext(companyId, groupId);
				long classNameIdJournalArticle = webContentUtil.getJournalClassNameId();
				JournalFolder journalFolder = journalFolderCreatorHelper.getOrCreateJournalFolder(companyId, groupId, folderName, StringPool.BLANK);
				long globalGroupId = webContentUtil.getGlobalGroupId(companyId);
				DDMStructure ddmStructure = ddmStructureLocalService.fetchStructure(globalGroupId, classNameIdJournalArticle, structureKey);
				if (Validator.isNull(ddmStructure) && WebContentStructure.EMAIL.getValue().equals(structureKey)) {
					ddmStructure = ddmStructureCreatorHelper.getOrCreateWebContentStructure(companyId, WebContentStructure.EMAIL);
				}
				ddmStructure = webContentUtil.checkWebContentStructure(serviceContext, globalGroupId, ddmStructure);
				String ddmTemplateKey = webContentUtil.getDefaultTemplateKeyForStructure(ddmStructure);
				Group group = groupLocalService.getGroup(groupId);

				for (Entry<String, InputStream> entry : inputStreams.entrySet()) {
					String articleFileName = entry.getKey();
					String articleId = inputOutputUtil.getKeyFromFileName(articleFileName);
					String content = inputOutputUtil.getLocalizedContentFromInputStream(group, entry.getValue());
					webContentUtil.getOrCreateJournalArticle(serviceContext, articleId, articleFileName, content, journalFolder, ddmStructure, ddmTemplateKey, true);
				}
			}
		} catch (Exception e) {
			throw new JournalCreationException("Error configuring web content articles", e);
		}
	}

	@Override
	public void createMissingJournalArticles(long companyId, long groupId, String folderName, Map<String, InputStream> inputStreams) throws JournalCreationException {
		createMissingJournalArticles(companyId, groupId, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY, folderName, inputStreams);
	}

	private JournalArticle getOrCreateJournalArticle(Group group, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleFileName, InputStream inputStream)
			throws JournalCreationException {
		try {
			String articleId = inputOutputUtil.getKeyFromFileName(articleFileName);
			String content = inputOutputUtil.getLocalizedContentFromInputStream(group, inputStream);
			return getOrCreateJournalArticle(group.getCompanyId(), group.getGroupId(), ddmStructure, journalFolder, skipWorkflow, articleId, articleFileName, content);
		} catch (Exception e) {
			LOG.error("Exception creating new webcontent article", e);
			throw new JournalCreationException("Exception creating new webcontent article", e);
		}
	}

	private JournalArticle getOrCreateJournalArticle(long companyId, long groupId, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleId, String title,
			String content) throws JournalCreationException {
		try {
			ServiceContext serviceContext = webContentUtil.getServiceContext(companyId, groupId);
			long globalGroupId = webContentUtil.getGlobalGroupId(companyId);
			DDMStructure journalStructure = webContentUtil.checkWebContentStructure(serviceContext, globalGroupId, ddmStructure);
			String ddmTemplateKey = webContentUtil.getDefaultTemplateKeyForStructure(journalStructure);

			return webContentUtil.getOrCreateJournalArticle(serviceContext, articleId, title, content, journalFolder, journalStructure, ddmTemplateKey, skipWorkflow);
		} catch (Exception e) {
			LOG.error("Exception creating new webcontent article", e);
			throw new JournalCreationException("Exception creating new webcontent article", e);
		}
	}

	@Reference
	protected void setDDMStructureCreatorHelper(DDMStructureCreatorHelper ddmStructureCreatorHelper) {
		this.ddmStructureCreatorHelper = ddmStructureCreatorHelper;
	}

	@Reference
	protected void setDDMStructureLocalService(DDMStructureLocalService dDMStructureLocalService) {
		ddmStructureLocalService = dDMStructureLocalService;
	}

	@Reference
	protected void setGroupLocalService(GroupLocalService groupLocalService) {
		this.groupLocalService = groupLocalService;
	}

	@Reference
	protected void setJournalFolderCreatorHelper(JournalFolderCreatorHelper journalFolderCreatorHelper) {
		this.journalFolderCreatorHelper = journalFolderCreatorHelper;
	}

	@Reference
	protected void setWebContentUtil(WebContentUtil webContentUtil) {
		this.webContentUtil = webContentUtil;
	}

	@Reference
	protected void setInputOutputUtil(InputOuputUtil inputOutputUtil) {
		this.inputOutputUtil = inputOutputUtil;
	}

}