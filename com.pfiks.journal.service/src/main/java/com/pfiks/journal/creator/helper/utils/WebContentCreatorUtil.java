/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.utils;

import java.io.InputStream;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DDM;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.journal.exception.JournalCreationException;

@Component(immediate = true, service = WebContentCreatorUtil.class)
public class WebContentCreatorUtil {

	private static final Log LOG = LogFactoryUtil.getLog(WebContentCreatorUtil.class);

	private DDM ddm;
	private DDMStructureLocalService ddmStructureLocalStructure;
	private DDMTemplateLocalService ddmTemplateLocalService;
	private InputOuputUtil inputOutputUtil;
	private LocaleMapUtil localeMapUtil;
	private ServiceContextHelper serviceContextHelper;

	public DDMStructure getOrCreateDDMStructure(long companyId, long groupId, long userId, String structureName, String structureKey, InputStream inputStream) throws JournalCreationException {
		try {
			long jaClassNameId = PortalUtil.getClassNameId(JournalArticle.class);

			Optional<DDMStructure> structureFound = getDDMStructure(groupId, jaClassNameId, structureKey);
			if (structureFound.isPresent()) {
				LOG.info("Web content structure already exists with structureKey:" + structureKey);
				return structureFound.get();
			} else {
				String definition = inputOutputUtil.getStringFromInputStream(inputStream);
				definition = StringUtil.replace(definition, "[$LOCALE_DEFAULT$]", LocaleUtil.getSiteDefault().toString());
				DDMForm ddmForm = ddm.getDDMForm(definition);
				Map<Locale, String> nameMap = localeMapUtil.getLocaleMapForAvailaleLocales(structureName);
				DDMFormLayout ddmFormLayout = ddm.getDefaultDDMFormLayout(ddmForm);
				DDMStructure structure = ddmStructureLocalStructure.addStructure(userId, groupId, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, jaClassNameId, structureKey, nameMap, nameMap,
						ddmForm, ddmFormLayout, "json", DDMStructureConstants.TYPE_DEFAULT, serviceContextHelper.getServiceContext(companyId, groupId, userId));
				LOG.info("Created new ddmStructure with structureKey: " + structureKey);
				return structure;
			}
		} catch (Exception e) {
			LOG.error("Exception creating new webcontent structure", e);
			throw new JournalCreationException("Exception creating new webcontent structure", e);
		}
	}

	public DDMTemplate getOrCreateDDMTemplate(long companyId, long groupId, long userId, String templateName, InputStream inputStream, String templateKey, DDMStructure ddmStructure)
			throws JournalCreationException {
		if (Validator.isNull(ddmStructure)) {
			throw new JournalCreationException("Exception creating new webcontent template. No structure found");
		}
		try {
			long structureClassnameId = PortalUtil.getClassNameId(DDMStructure.class);
			long jaClassNameId = PortalUtil.getClassNameId(JournalArticle.class);

			DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(groupId, structureClassnameId, templateKey);
			if (Validator.isNull(ddmTemplate)) {
				long classPK = ddmStructure.getStructureId();
				Map<Locale, String> nameMap = localeMapUtil.getLocaleMapForAvailaleLocales(templateName);
				String script = inputOutputUtil.getStringFromInputStream(inputStream);
				ddmTemplate = ddmTemplateLocalService.addTemplate(userId, groupId, structureClassnameId, classPK, jaClassNameId, templateKey, nameMap, nameMap,
						DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE, "ftl", script, true, false, null, null,
						serviceContextHelper.getServiceContext(companyId, groupId, userId));
				LOG.info("Created new ddmTemplate with templateKey: " + templateKey);
			}
			return ddmTemplate;
		} catch (Exception e) {
			throw new JournalCreationException("Unable to get or create template", e);
		}
	}

	public DDMTemplate getOrCreateDDMTemplateWithNoStructure(long companyId, long groupId, long userId, String templateName, InputStream inputStream, String templateKey)
			throws JournalCreationException {
		try {
			long structureClassnameId = PortalUtil.getClassNameId(DDMStructure.class);
			long jaClassNameId = PortalUtil.getClassNameId(JournalArticle.class);

			DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(groupId, structureClassnameId, templateKey);
			if (Validator.isNull(ddmTemplate)) {

				Map<Locale, String> nameMap = localeMapUtil.getLocaleMapForAvailaleLocales(templateName);
				String script = inputOutputUtil.getStringFromInputStream(inputStream);
				ddmTemplate = ddmTemplateLocalService.addTemplate(userId, groupId, structureClassnameId, 0, jaClassNameId, templateKey, nameMap, nameMap, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY,
						DDMTemplateConstants.TEMPLATE_MODE_CREATE, "ftl", script, true, false, null, null, serviceContextHelper.getServiceContext(companyId, groupId, userId));
				LOG.info("Created new ddmTemplate with templateKey: " + templateKey);
			}
			return ddmTemplate;
		} catch (Exception e) {
			throw new JournalCreationException("Unable to get or create template", e);
		}
	}

	private Optional<DDMStructure> getDDMStructure(long groupId, long classNameId, String structureKey) {
		try {
			return Optional.of(ddmStructureLocalStructure.getStructure(groupId, classNameId, structureKey));
		} catch (Exception e) {
			LOG.warn("Exception retrieving DDMStructure with groupId: " + groupId + ", classNameId: " + classNameId + ", structureKey: " + structureKey + " - " + e.getMessage());
			return Optional.empty();
		}
	}

	@Reference
	protected void setDdm(DDM ddm) {
		this.ddm = ddm;
	}

	@Reference
	protected void setDDMStructureLocalStructure(DDMStructureLocalService ddmStructureLocalStructure) {
		this.ddmStructureLocalStructure = ddmStructureLocalStructure;
	}

	@Reference
	public void setDDMTemplateLocalService(DDMTemplateLocalService ddmTemplateLocalService) {
		this.ddmTemplateLocalService = ddmTemplateLocalService;
	}

	@Reference
	protected void setInputOutputUtil(InputOuputUtil inputOutputUtil) {
		this.inputOutputUtil = inputOutputUtil;
	}

	@Reference
	protected void setLocaleMapUtil(LocaleMapUtil localeMapUtil) {
		this.localeMapUtil = localeMapUtil;
	}

	@Reference
	protected void setServiceContextHelper(ServiceContextHelper serviceContextHelper) {
		this.serviceContextHelper = serviceContextHelper;
	}
}
