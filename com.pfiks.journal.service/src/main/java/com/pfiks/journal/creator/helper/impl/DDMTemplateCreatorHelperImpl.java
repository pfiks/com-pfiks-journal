/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.impl;

import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.PortalUtil;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.journal.creator.helper.DDMTemplateCreatorHelper;
import com.pfiks.journal.creator.helper.utils.WebContentCreatorUtil;
import com.pfiks.journal.exception.JournalCreationException;

@Component(immediate = true, service = DDMTemplateCreatorHelper.class)
public class DDMTemplateCreatorHelperImpl implements DDMTemplateCreatorHelper {

	private InputOuputUtil inputOutputUtil;
	private DDMStructureLocalService ddmStructureLocalStructure;
	private WebContentCreatorUtil webContentCreatorUtil;

	@Override
	public void createMissingWebContentTemplate(long companyId, long groupId, long userId, String templateFileName, InputStream inputStream) throws JournalCreationException {
		long journalClassNameId = PortalUtil.getClassNameId(JournalArticle.class);
		String templateKey = inputOutputUtil.getKeyFromFileName(templateFileName);
		String structureName = StringUtils.substringBeforeLast(templateFileName, StringPool.DASH);
		String structureKey = inputOutputUtil.getKeyFromFileName(StringUtils.trim(structureName));
		DDMStructure ddmStructure = ddmStructureLocalStructure.fetchStructure(groupId, journalClassNameId, structureKey);
		webContentCreatorUtil.getOrCreateDDMTemplate(companyId, groupId, userId, templateFileName, inputStream, templateKey, ddmStructure);
	}

	@Override
	public void createMissingWebContentTemplates(DDMStructure ddmStructure, Map<String, InputStream> templates) throws JournalCreationException {
		for (Entry<String, InputStream> template : templates.entrySet()) {
			String templateKey = inputOutputUtil.getKeyFromFileName(template.getKey());
			webContentCreatorUtil.getOrCreateDDMTemplate(ddmStructure.getCompanyId(), ddmStructure.getGroupId(), ddmStructure.getUserId(), template.getKey(), template.getValue(), templateKey,
					ddmStructure);
		}
	}

	@Override
	public DDMTemplate getOrCreateWebContentTemplate(Group group, DDMStructure ddmStructure, String templateName, String templateKey, InputStream inputStream) throws JournalCreationException {
		return webContentCreatorUtil.getOrCreateDDMTemplate(group.getCompanyId(), group.getGroupId(), group.getCreatorUserId(), templateName, inputStream, templateKey, ddmStructure);
	}

	@Override
	public DDMTemplate getOrCreateWebContentTemplateWithNoStructure(Group group, String templateName, String templateKey, InputStream inputStream) throws JournalCreationException {
		return webContentCreatorUtil.getOrCreateDDMTemplateWithNoStructure(group.getCompanyId(), group.getGroupId(), group.getCreatorUserId(), templateName, inputStream, templateKey);
	}

	@Reference
	protected void setInputOutputUtil(InputOuputUtil inputOutputUtil) {
		this.inputOutputUtil = inputOutputUtil;
	}

	@Reference
	protected void setDdmStructureLocalStructure(DDMStructureLocalService ddmStructureLocalStructure) {
		this.ddmStructureLocalStructure = ddmStructureLocalStructure;
	}

	@Reference
	protected void setWebContentCreatorUtil(WebContentCreatorUtil webContentCreatorUtil) {
		this.webContentCreatorUtil = webContentCreatorUtil;
	}
}
