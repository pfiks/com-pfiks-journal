/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalFolderLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.journal.creator.helper.JournalFolderCreatorHelper;
import com.pfiks.journal.exception.JournalCreationException;

@Component(immediate = true, property = {}, service = JournalFolderCreatorHelper.class)
public class JournalFolderCreatorHelperImpl implements JournalFolderCreatorHelper {

	private static final Log LOG = LogFactoryUtil.getLog(JournalFolderCreatorHelperImpl.class);

	private ServiceContextHelper serviceContextHelper;
	private UserLocalService userLocalService;
	private JournalFolderLocalService journalFolderLocalService;

	@Override
	public JournalFolder getOrCreateJournalFolder(long companyId, long groupId, String folderName, String folderDescription) throws JournalCreationException {
		JournalFolder journalFolder = null;
		if (Validator.isNotNull(folderName)) {
			journalFolder = getJournalFolder(groupId, folderName);
			if (Validator.isNull(journalFolder)) {
				journalFolder = createJournalFolder(companyId, groupId, folderName, folderDescription);
			}
		}
		return journalFolder;
	}

	private JournalFolder getJournalFolder(long groupId, String folderName) {
		return journalFolderLocalService.fetchFolder(groupId, folderName);
	}

	private JournalFolder createJournalFolder(long companyId, long groupId, String folderName, String folderDescription) throws JournalCreationException {
		try {
			long defaultUserId = userLocalService.getDefaultUserId(companyId);
			JournalFolder journalFolder = journalFolderLocalService.addFolder(defaultUserId, groupId, 0, folderName, folderDescription,
					serviceContextHelper.getServiceContext(companyId, groupId, defaultUserId));
			LOG.info("JournalFolder correctly created in groupId: " + groupId + ", folderName: " + folderName);
			return journalFolder;
		} catch (Exception e) {
			LOG.error("Exception creating JournalFolder", e);
			throw new JournalCreationException("Exception creating journal folder", e);
		}
	}

	@Reference
	public void setServiceContextHelper(ServiceContextHelper serviceContextHelper) {
		this.serviceContextHelper = serviceContextHelper;
	}

	@Reference
	public void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	@Reference
	public void setJournalFolderLocalService(JournalFolderLocalService journalFolderLocalService) {
		this.journalFolderLocalService = journalFolderLocalService;
	}

}