/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.utils.constants;

public class CreationConstants {

	public static final String BASIC_WEB_CONTENT_STRUCTURE_KEY = "BASIC-WEB-CONTENT";

	public static final String BASIC_WEB_CONTENT_TEMPLATE_KEY = "BASIC-WEB-CONTENT";

	private CreationConstants() {
	}

}
