/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.impl;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.journal.constants.WebContentStructure;
import com.pfiks.journal.creator.helper.DDMStructureCreatorHelper;
import com.pfiks.journal.creator.helper.DDMTemplateCreatorHelper;
import com.pfiks.journal.creator.helper.utils.WebContentCreatorUtil;
import com.pfiks.journal.exception.JournalCreationException;
import com.pfiks.journal.service.impl.JournalFileService;

@Component(immediate = true, service = DDMStructureCreatorHelper.class)
public class DDMStructureCreatorHelperImpl implements DDMStructureCreatorHelper {

	private DDMTemplateCreatorHelper ddmTemplateCreatorHelper;
	private GroupLocalService groupLocalService;
	private InputOuputUtil inputOutputUtil;
	private JournalFileService journalFileService;
	private WebContentCreatorUtil webContentCreatorUtil;

	@Override
	public void createMissingWebContentStructure(long companyId, long groupId, long userId, String structureFileName, InputStream inputStream) throws JournalCreationException {
		String structureKey = inputOutputUtil.getKeyFromFileName(structureFileName);
		webContentCreatorUtil.getOrCreateDDMStructure(companyId, groupId, userId, structureFileName, structureKey, inputStream);
	}

	@Override
	public DDMStructure getOrCreateWebContentStructure(long companyId, WebContentStructure structureName) throws JournalCreationException {
		try {
			Group companyGroup = groupLocalService.getCompanyGroup(companyId);
			long globalGroupId = companyGroup.getGroupId();
			long userId = companyGroup.getCreatorUserId();

			InputStream structureFile = journalFileService.getStructureFile(structureName);
			DDMStructure ddmStructure = webContentCreatorUtil.getOrCreateDDMStructure(companyId, globalGroupId, userId, structureName.getValue(), structureName.getValue(), structureFile);

			InputStream structureTemplate = journalFileService.getStructureTemplate(structureName);
			ddmTemplateCreatorHelper.createMissingWebContentTemplate(companyId, globalGroupId, userId, structureName.getValue(), structureTemplate);

			return ddmStructure;
		} catch (Exception e) {
			throw new JournalCreationException("Exception creating new webcontent structure and template", e);
		}
	}

	@Override
	public DDMStructure getOrCreateWebContentStructure(Group group, String structureName, String structureKey, InputStream inputStream) throws JournalCreationException {
		return webContentCreatorUtil.getOrCreateDDMStructure(group.getCompanyId(), group.getGroupId(), group.getCreatorUserId(), structureName, structureKey, inputStream);
	}

	@Reference
	protected void setDdmTemplateCreatorHelper(DDMTemplateCreatorHelper ddmTemplateCreatorHelper) {
		this.ddmTemplateCreatorHelper = ddmTemplateCreatorHelper;
	}

	@Reference
	protected void setJournalFileService(JournalFileService journalFileService) {
		this.journalFileService = journalFileService;
	}

	@Reference
	protected void setGroupLocalService(GroupLocalService groupLocalService) {
		this.groupLocalService = groupLocalService;
	}

	@Reference
	protected void setInputOutputUtil(InputOuputUtil inputOutputUtil) {
		this.inputOutputUtil = inputOutputUtil;
	}

	@Reference
	protected void setWebContentCreatorUtil(WebContentCreatorUtil webContentCreatorUtil) {
		this.webContentCreatorUtil = webContentCreatorUtil;
	}

}
