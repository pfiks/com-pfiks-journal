/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.utils;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.annotations.DDMForm;
import com.liferay.dynamic.data.mapping.annotations.DDMFormLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.journal.exception.NoSuchArticleException;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.workflow.WorkflowThreadLocal;
import com.pfiks.common.exception.CommonUtilsException;
import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.journal.creator.helper.utils.constants.CreationConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, WorkflowThreadLocal.class })
public class WebContentUtilTest {

	private static final long GROUP_ID = 1L;
	private static final long COMPANY_ID = 2L;
	private static final long USER_ID = 3L;
	private static final long FOLDER_ID = 4L;
	private static final long JOURNAL_CLASS_NAME_ID = 5l;
	private static final long STRUCTURE_CLASS_NAME_ID = 125l;
	private static final Long GLOBAL_GROUP_ID = 6L;
	private static final Long GLOBAL_GROUP_ID_DEFAULT = 8l;
	private static final Long COMPANY_ID_DEFAULT = 7l;
	private static final String JOURNAL_ARTICLE_TITLE = "myJournalArticleTitle";
	private static final String JOURNAL_ARTICLE_ID = "myJournalArticleId";
	private static final String JOURNAL_ARTICLE_CONTENT = "myJournalArticleContent";
	private static final String STRUCTURE_KEY = "structureKey";
	private static final String TEMPLATE_KEY = "templateKey";
	private static final String STORAGE_TYPE = "storageType";
	private static final Integer TYPE = 65;
	private int displayDateMonth;
	private int displayDateDay;
	private int displayDateYear;

	private WebContentUtil webContentUtil;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private LocaleMapUtil mockLocaleMapUtil;

	@Mock
	private ServiceContextHelper mockServiceContextHelper;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructure mockDDMStructureCreated;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private DDMTemplate mockDDMTemplateOne;

	@Mock
	private DDMTemplate mockDDMTemplateTwo;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Map<Locale, String> mockTitleMap;

	@Mock
	private Map<Locale, String> mockDescriptionMap;

	@Mock
	private Map<Locale, String> mockTitleMapTwo;

	@Mock
	private Map<Locale, String> mockDescriptionMapTwo;

	@Mock
	private com.liferay.dynamic.data.mapping.model.DDMForm mockDDMForm;

	@Mock
	private com.liferay.dynamic.data.mapping.model.DDMFormLayout mockDDMFormLayout;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(PortalUtil.class, WorkflowThreadLocal.class);

		webContentUtil = new WebContentUtil();
		webContentUtil.setDDMStructureLocalService(mockDDMStructureLocalService);
		webContentUtil.setDDMTemplateLocalService(mockDDMTemplateLocalService);
		webContentUtil.setGroupLocalService(mockGroupLocalService);
		webContentUtil.setJournalArticleLocalService(mockJournalArticleLocalService);
		webContentUtil.setLocaleMapUtil(mockLocaleMapUtil);
		webContentUtil.setServiceContextHelper(mockServiceContextHelper);
		webContentUtil.setUserLocalService(mockUserLocalService);
	}

	@Test
	public void getDefaultTemplateKeyForStructure_WhenStructureIsNull_ThenReturnsBasicWebContentTemplateKey() throws PortalException {
		String result = webContentUtil.getDefaultTemplateKeyForStructure(null);

		assertThat(result, equalTo(CreationConstants.BASIC_WEB_CONTENT_TEMPLATE_KEY));
	}

	@Test
	public void getDefaultTemplateKeyForStructure_WhenStructureHasNoTemplates_ThenReturnsBasicWebContentTemplateKey() throws PortalException {
		when(mockDDMStructure.getTemplates()).thenReturn(Collections.emptyList());

		String result = webContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructure);

		assertThat(result, equalTo(CreationConstants.BASIC_WEB_CONTENT_TEMPLATE_KEY));
	}

	@Test
	public void getDefaultTemplateKeyForStructure_WhenStructureHasTemplates_ThenReturnsTheKeyForTheFirstTemplate() throws PortalException {
		when(mockDDMTemplateOne.getTemplateKey()).thenReturn(TEMPLATE_KEY);
		when(mockDDMTemplateTwo.getTemplateKey()).thenReturn("differentKey");

		List<DDMTemplate> templates = new LinkedList<>();
		templates.add(mockDDMTemplateOne);
		templates.add(mockDDMTemplateTwo);
		when(mockDDMStructure.getTemplates()).thenReturn(templates);

		String result = webContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructure);

		assertThat(result, equalTo(TEMPLATE_KEY));
	}

	@Test
	public void getJournalClassNameId_WhenNoError_ThenReturnsJournalArticleClassNameId() {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);

		long result = webContentUtil.getJournalClassNameId();

		assertThat(result, equalTo(JOURNAL_CLASS_NAME_ID));
	}

	@Test
	public void getGlobalGroupId_WhenNoError_ThenReturnsTheGlobalGroupId() throws PortalException {
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GLOBAL_GROUP_ID);

		long result = webContentUtil.getGlobalGroupId(COMPANY_ID);

		assertThat(result, equalTo(GLOBAL_GROUP_ID));
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnsServiceContext() throws PortalException {
		when(mockUserLocalService.getDefaultUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockServiceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID)).thenReturn(mockServiceContext);

		ServiceContext result = webContentUtil.getServiceContext(COMPANY_ID, GROUP_ID);

		assertThat(result, sameInstance(mockServiceContext));
	}

	@Test
	public void checkWebContentStructure_WhenStructureIsValid_ThenNoActionIsPerformed() throws PortalException {
		webContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure);

		verifyZeroInteractions(mockDDMStructureLocalService, mockGroupLocalService, mockDDMTemplateLocalService);
	}

	@Test
	public void checkWebContentStructure_WhenStructureIsValid_ThenReturnsTheStructure() throws PortalException {
		DDMStructure result = webContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Test
	public void checkWebContentStructure_WhenStructureIsNullAndBasicWebContentStructureAlreadyExistsInTheGlobalGroupForTheCompany_ThenReturnsTheStructureRetrieved() throws PortalException {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GLOBAL_GROUP_ID, JOURNAL_CLASS_NAME_ID, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		DDMStructure result = webContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, null);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Test
	public void checkWebContentStructure_WhenStructureIsNullAndBasicWebContentStructureAlreadyExistsInTheGlobalGroupForTheCompany_ThenNoStructureIsCreated() throws PortalException {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GLOBAL_GROUP_ID, JOURNAL_CLASS_NAME_ID, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		webContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, null);

		verify(mockDDMStructureLocalService, never()).addStructure(anyLong(), anyLong(), anyLong(), anyLong(), anyString(), anyMap(), anyMap(),
				(com.liferay.dynamic.data.mapping.model.DDMForm) any(DDMForm.class), (com.liferay.dynamic.data.mapping.model.DDMFormLayout) any(DDMFormLayout.class), anyString(), anyInt(),
				any(ServiceContext.class));
	}

	@Test
	public void checkWebContentStructure_WhenStructureIsNullAndBasicWebContentStructureAlreadyExistsInTheGlobalGroupForTheCompany_ThenNoTemplateIsCreated() throws PortalException {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GLOBAL_GROUP_ID, JOURNAL_CLASS_NAME_ID, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		webContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, null);

		verifyZeroInteractions(mockDDMTemplateLocalService);
	}

	@Test
	public void checkWebContentStructure_WhenStructureIsNullAndBasicWebContentStructureIsNotFoundInTheGlobalGroupForTheCompany_ThenReturnsTheBasicWebContentStructureNewlyCreatedFromTheBasicWebContentStructureInTheDefaultCompany()
			throws PortalException {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GLOBAL_GROUP_ID, JOURNAL_CLASS_NAME_ID, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY)).thenReturn(null);
		when(PortalUtil.getDefaultCompanyId()).thenReturn(COMPANY_ID_DEFAULT);
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID_DEFAULT)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GLOBAL_GROUP_ID_DEFAULT);
		when(mockDDMStructureLocalService.getStructure(GLOBAL_GROUP_ID_DEFAULT, JOURNAL_CLASS_NAME_ID, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockDDMStructure.getStructureKey()).thenReturn(STRUCTURE_KEY);
		when(mockDDMStructure.getNameMap()).thenReturn(mockTitleMap);
		when(mockDDMStructure.getDescriptionMap()).thenReturn(mockDescriptionMap);
		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMStructure.getDDMFormLayout()).thenReturn(mockDDMFormLayout);
		when(mockDDMStructure.getStorageType()).thenReturn(STORAGE_TYPE);
		when(mockDDMStructure.getType()).thenReturn(TYPE);
		List<DDMTemplate> templates = new LinkedList<>();
		templates.add(mockDDMTemplateOne);
		templates.add(mockDDMTemplateTwo);
		when(mockDDMStructure.getTemplates()).thenReturn(templates);
		when(mockDDMStructureLocalService.addStructure(USER_ID, GLOBAL_GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, JOURNAL_CLASS_NAME_ID, STRUCTURE_KEY, mockTitleMap,
				mockDescriptionMap, mockDDMForm, mockDDMFormLayout, STORAGE_TYPE, TYPE, mockServiceContext)).thenReturn(mockDDMStructureCreated);

		DDMStructure result = webContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, null);

		assertThat(result, sameInstance(mockDDMStructureCreated));
	}

	@Test
	public void checkWebContentStructure_WhenStructureIsNullAndBasicWebContentStructureIsNotFoundInTheGlobalGroupForTheCompany_ThenCreatesTheBasicWebContentTemplateFromTheBasicWebContentTemplateInTheDefaultCompany()
			throws PortalException {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(STRUCTURE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.fetchStructure(GLOBAL_GROUP_ID, JOURNAL_CLASS_NAME_ID, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY)).thenReturn(null);
		when(PortalUtil.getDefaultCompanyId()).thenReturn(COMPANY_ID_DEFAULT);
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID_DEFAULT)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GLOBAL_GROUP_ID_DEFAULT);
		when(mockDDMStructureLocalService.getStructure(GLOBAL_GROUP_ID_DEFAULT, JOURNAL_CLASS_NAME_ID, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockDDMStructure.getStructureKey()).thenReturn(STRUCTURE_KEY);
		Long createdStructureId = 4444l;
		when(mockDDMStructureCreated.getStructureId()).thenReturn(createdStructureId);
		when(mockDDMStructure.getNameMap()).thenReturn(mockTitleMap);
		when(mockDDMStructure.getDescriptionMap()).thenReturn(mockDescriptionMap);
		when(mockDDMStructure.getDDMForm()).thenReturn(mockDDMForm);
		when(mockDDMStructure.getDDMFormLayout()).thenReturn(mockDDMFormLayout);
		when(mockDDMStructure.getStorageType()).thenReturn(STORAGE_TYPE);
		when(mockDDMStructure.getType()).thenReturn(TYPE);
		List<DDMTemplate> templates = new LinkedList<>();
		templates.add(mockDDMTemplateOne);
		templates.add(mockDDMTemplateTwo);
		String templateType = "templateType";
		String templateMode = "templateMode";
		String templateLanguage = "templateLanguage";
		String templateScript = "templateScript";
		Boolean templateOneCache = true;
		when(mockDDMTemplateOne.getNameMap()).thenReturn(mockTitleMapTwo);
		when(mockDDMTemplateOne.getDescriptionMap()).thenReturn(mockDescriptionMapTwo);
		when(mockDDMTemplateOne.getType()).thenReturn(templateType);
		when(mockDDMTemplateOne.getMode()).thenReturn(templateMode);
		when(mockDDMTemplateOne.getLanguage()).thenReturn(templateLanguage);
		when(mockDDMTemplateOne.isCacheable()).thenReturn(templateOneCache);
		Boolean templateOneImage = false;
		when(mockDDMTemplateOne.getSmallImage()).thenReturn(templateOneImage);
		when(mockDDMTemplateOne.getScript()).thenReturn(templateScript);
		String templateOneImageURL = "templateOneImageURL";
		when(mockDDMTemplateOne.getSmallImageURL()).thenReturn(templateOneImageURL);
		when(mockDDMStructure.getTemplates()).thenReturn(templates);
		when(mockDDMStructureLocalService.addStructure(USER_ID, GLOBAL_GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, JOURNAL_CLASS_NAME_ID, STRUCTURE_KEY, mockTitleMap,
				mockDescriptionMap, mockDDMForm, mockDDMFormLayout, STORAGE_TYPE, TYPE, mockServiceContext)).thenReturn(mockDDMStructureCreated);

		webContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, null);

		verify(mockDDMTemplateLocalService, times(1)).addTemplate(USER_ID, GLOBAL_GROUP_ID, STRUCTURE_CLASS_NAME_ID, createdStructureId, JOURNAL_CLASS_NAME_ID,
				CreationConstants.BASIC_WEB_CONTENT_TEMPLATE_KEY, mockTitleMapTwo, mockDescriptionMapTwo, templateType, templateMode, templateLanguage, templateScript, templateOneCache,
				templateOneImage, templateOneImageURL, null, mockServiceContext);
	}

	@Test
	public void getOrCreateJournalArticle_WhenJournalFolderIsNullAndNoArticleIsFound_ThenAnewJournalArticleIsCreatedInTheDefaultFolder() throws PortalException, CommonUtilsException {
		mockArticleCreation();

		webContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT, null, mockDDMStructure, TEMPLATE_KEY, true);

		verify(mockJournalArticleLocalService, times(1)).addArticle(USER_ID, GROUP_ID, JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID, 0, 0, JOURNAL_ARTICLE_ID, false, 1.0, mockTitleMap, null,
				JOURNAL_ARTICLE_CONTENT, STRUCTURE_KEY, TEMPLATE_KEY, null, displayDateMonth, displayDateDay, displayDateYear, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, 0, 0, 0, true, true, false,
				StringPool.BLANK, null, null, null, mockServiceContext);
	}

	@Test
	public void getOrCreateJournalArticle_WhenFolderIsValidAndJournalArticleAlreadyExistsWithSameArticleId_ThenNoArticleIsCreated() throws PortalException, CommonUtilsException {
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, JOURNAL_ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);

		webContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT, mockJournalFolder, mockDDMStructure, TEMPLATE_KEY, true);

		verify(mockJournalArticleLocalService, never()).addArticle(anyLong(), anyLong(), anyLong(), anyLong(), anyLong(), anyString(), anyBoolean(), anyDouble(), anyMap(), anyMap(), anyString(),
				anyString(), anyString(), anyString(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyInt(), anyBoolean(), anyInt(), anyInt(), anyInt(),
				anyInt(), anyInt(), anyBoolean(), anyBoolean(), anyBoolean(), anyString(), any(File.class), anyMap(), anyString(), any(ServiceContext.class));
	}

	@Test
	public void getOrCreateJournalArticle_WhenJournalArticleAlreadyExistsWithSameArticleId_ThenReturnsTheArticleFound() throws PortalException, CommonUtilsException {
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, JOURNAL_ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);

		JournalArticle result = webContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT, mockJournalFolder, mockDDMStructure,
				TEMPLATE_KEY, true);

		assertThat(result, sameInstance(mockJournalArticle));
	}

	@Test
	public void getOrCreateJournalArticle_WhenNoJournalArticleExistsWithSameArticleId_ThenReturnsThenewJournalArticle() throws PortalException, CommonUtilsException {
		mockArticleCreation();

		JournalArticle result = webContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT, mockJournalFolder, mockDDMStructure,
				TEMPLATE_KEY, true);

		assertThat(result, sameInstance(mockJournalArticle));
	}

	@Test
	public void getOrCreateJournalArticle_WhenFolderIsValidAndNoJournalArticleExistsWithSameArticleId_ThenAnewJournalArticleIsCreated() throws PortalException, CommonUtilsException {
		mockArticleCreation();

		webContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT, mockJournalFolder, mockDDMStructure, TEMPLATE_KEY, true);

		verify(mockJournalArticleLocalService, times(1)).addArticle(USER_ID, GROUP_ID, FOLDER_ID, 0, 0, JOURNAL_ARTICLE_ID, false, 1.0, mockTitleMap, null, JOURNAL_ARTICLE_CONTENT, STRUCTURE_KEY,
				TEMPLATE_KEY, null, displayDateMonth, displayDateDay, displayDateYear, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, 0, 0, 0, true, true, false, StringPool.BLANK, null, null, null,
				mockServiceContext);
	}

	@Test
	public void createArticle_WhenArticleIsCreatedAndSkipWorkflowIsFalse_ThenDoesNotDisableWorkflowThreadLocal() throws PortalException, CommonUtilsException {
		mockArticleCreation();
		boolean skipWorkflow = false;
		boolean workflowEnabled = true;
		when(WorkflowThreadLocal.isEnabled()).thenReturn(workflowEnabled);

		webContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT, mockJournalFolder, mockDDMStructure, TEMPLATE_KEY,
				skipWorkflow);

		verifyStatic(never());
		WorkflowThreadLocal.setEnabled(false);
	}

	@Test
	public void createArticle_WhenArticleIsCreatedAndSkipWorkflowIsTrueAndWorkflowIsNotEnabled_ThenDoesNotDisableWorkflowThreadLocal() throws PortalException, CommonUtilsException {
		mockArticleCreation();
		boolean skipWorkflow = true;
		boolean workflowEnabled = false;
		when(WorkflowThreadLocal.isEnabled()).thenReturn(workflowEnabled);

		webContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT, mockJournalFolder, mockDDMStructure, TEMPLATE_KEY,
				skipWorkflow);

		verifyStatic(never());
		WorkflowThreadLocal.setEnabled(false);
	}

	@Test
	public void createArticle_WhenArticleIsCreatedAndSkipWorkflowIsTrueAndWorkflowIsEnabled_ThenDisableTheWorkflowAndReenablesIt() throws PortalException, CommonUtilsException {
		mockArticleCreation();
		boolean skipWorkflow = true;
		boolean workflowEnabled = true;
		when(WorkflowThreadLocal.isEnabled()).thenReturn(workflowEnabled);

		webContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID, JOURNAL_ARTICLE_TITLE, JOURNAL_ARTICLE_CONTENT, mockJournalFolder, mockDDMStructure, TEMPLATE_KEY,
				skipWorkflow);

		verifyStatic(times(1));
		WorkflowThreadLocal.setEnabled(false);

		verifyStatic(times(1));
		WorkflowThreadLocal.setEnabled(true);
	}

	private void mockArticleCreation() throws CommonUtilsException, PortalException {
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, JOURNAL_ARTICLE_ID)).thenThrow(new NoSuchArticleException());
		when(mockLocaleMapUtil.getLocaleMapForAvailaleLocales(JOURNAL_ARTICLE_TITLE)).thenReturn(mockTitleMap);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockJournalFolder.getFolderId()).thenReturn(FOLDER_ID);
		when(mockDDMStructure.getStructureKey()).thenReturn(STRUCTURE_KEY);
		Calendar calendar = Calendar.getInstance();
		displayDateMonth = calendar.get(Calendar.MONTH);
		displayDateDay = calendar.get(Calendar.DAY_OF_MONTH);
		displayDateYear = calendar.get(Calendar.YEAR);

		when(mockJournalArticleLocalService.addArticle(USER_ID, GROUP_ID, FOLDER_ID, 0, 0, JOURNAL_ARTICLE_ID, false, 1.0, mockTitleMap, null, JOURNAL_ARTICLE_CONTENT, STRUCTURE_KEY, TEMPLATE_KEY,
				null, displayDateMonth, displayDateDay, displayDateYear, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, 0, 0, 0, true, true, false, StringPool.BLANK, null, null, null, mockServiceContext))
						.thenReturn(mockJournalArticle);
	}
}
