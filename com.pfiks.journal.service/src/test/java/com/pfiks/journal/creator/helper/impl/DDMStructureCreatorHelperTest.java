/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.journal.constants.WebContentStructure;
import com.pfiks.journal.creator.helper.DDMTemplateCreatorHelper;
import com.pfiks.journal.creator.helper.utils.WebContentCreatorUtil;
import com.pfiks.journal.exception.JournalCreationException;
import com.pfiks.journal.service.impl.JournalFileService;

public class DDMStructureCreatorHelperTest {

	private static final String STRUCTURE_KEY = WebContentStructure.EMAIL.getValue();
	private static final long COMPANY_ID = 1L;
	private static final long GROUP_ID = 2L;
	private static final long USER_ID = 3L;
	private static final String STRUCTURE_NAME = "structureFileName";

	private DDMStructureCreatorHelperImpl ddmStructureCreatorHelperImpl;

	@Mock
	private ServiceContextHelper mockServiceContextHelper;

	@Mock
	private LocaleMapUtil mockLocaleMapUtil;

	@Mock
	private JournalFileService mockJournalFileService;

	@Mock
	private DDMTemplateCreatorHelper mockDDMTemplateCreatorHelper;

	@Mock
	private WebContentCreatorUtil mockWebContentCreatorUtil;

	@Mock
	private InputOuputUtil mockInputOutputUtil;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private Group mockGroup;

	@Mock
	private InputStream mockInputStreamStructure;

	@Mock
	private InputStream mockInputStreamTemplate;

	@Mock
	private DDMStructure mockDDMStructure;

	@Before
	public void setUp() {
		initMocks(this);

		ddmStructureCreatorHelperImpl = new DDMStructureCreatorHelperImpl();
		ddmStructureCreatorHelperImpl.setDdmTemplateCreatorHelper(mockDDMTemplateCreatorHelper);
		ddmStructureCreatorHelperImpl.setGroupLocalService(mockGroupLocalService);
		ddmStructureCreatorHelperImpl.setInputOutputUtil(mockInputOutputUtil);
		ddmStructureCreatorHelperImpl.setJournalFileService(mockJournalFileService);
		ddmStructureCreatorHelperImpl.setWebContentCreatorUtil(mockWebContentCreatorUtil);
	}

	@Test
	public void createMissingWebContentStructure_WhenNoError_ThenCallsGetOrCreateDDMStructure() throws Exception {
		when(mockInputOutputUtil.getKeyFromFileName(STRUCTURE_NAME)).thenReturn(STRUCTURE_KEY);

		ddmStructureCreatorHelperImpl.createMissingWebContentStructure(COMPANY_ID, GROUP_ID, USER_ID, STRUCTURE_NAME, mockInputStreamStructure);

		verify(mockWebContentCreatorUtil, times(1)).getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, STRUCTURE_NAME, STRUCTURE_KEY, mockInputStreamStructure);
	}

	@Test
	public void getOrCreateWebContentStructure_WithWebcontentStructureParam_WhenNoError_ThenStructureIsCreatedAndReturned() throws Exception {
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCreatorUserId()).thenReturn(USER_ID);
		when(mockJournalFileService.getStructureFile(WebContentStructure.EMAIL)).thenReturn(mockInputStreamStructure);
		when(mockJournalFileService.getStructureTemplate(WebContentStructure.EMAIL)).thenReturn(mockInputStreamTemplate);
		when(mockWebContentCreatorUtil.getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, WebContentStructure.EMAIL.getValue(), WebContentStructure.EMAIL.getValue(), mockInputStreamStructure))
				.thenReturn(mockDDMStructure);

		DDMStructure result = ddmStructureCreatorHelperImpl.getOrCreateWebContentStructure(COMPANY_ID, WebContentStructure.EMAIL);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Test
	public void getOrCreateWebContentStructure_WithWebcontentStructureParam_WhenNoError_ThenCreatesStructureTemplate() throws Exception {
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCreatorUserId()).thenReturn(USER_ID);
		when(mockJournalFileService.getStructureFile(WebContentStructure.EMAIL)).thenReturn(mockInputStreamStructure);
		when(mockJournalFileService.getStructureTemplate(WebContentStructure.EMAIL)).thenReturn(mockInputStreamTemplate);
		when(mockWebContentCreatorUtil.getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, WebContentStructure.EMAIL.getValue(), WebContentStructure.EMAIL.getValue(), mockInputStreamStructure))
				.thenReturn(mockDDMStructure);
		when(mockJournalFileService.getStructureTemplate(WebContentStructure.EMAIL)).thenReturn(mockInputStreamTemplate);

		ddmStructureCreatorHelperImpl.getOrCreateWebContentStructure(COMPANY_ID, WebContentStructure.EMAIL);

		verify(mockDDMTemplateCreatorHelper, times(1)).createMissingWebContentTemplate(COMPANY_ID, GROUP_ID, USER_ID, WebContentStructure.EMAIL.getValue(), mockInputStreamTemplate);
	}

	@Test
	public void getOrCreateWebContentStructure_WithGroupParam_WhenNoError_ThenReturnsTheDDMStructure() throws JournalCreationException {
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getCreatorUserId()).thenReturn(USER_ID);
		when(mockWebContentCreatorUtil.getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, STRUCTURE_NAME, STRUCTURE_KEY, mockInputStreamStructure)).thenReturn(mockDDMStructure);

		DDMStructure result = mockWebContentCreatorUtil.getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, STRUCTURE_NAME, STRUCTURE_KEY, mockInputStreamStructure);

		assertThat(result, sameInstance(mockDDMStructure));

	}

}
