/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.PortalUtil;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.journal.creator.helper.utils.WebContentCreatorUtil;
import com.pfiks.journal.exception.JournalCreationException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, LanguageUtil.class })
public class DDMTemplateCreatorHelperTest {

	private static final String STRUCTURE_KEY = "myStructureOneKey";
	private static final String STRUCTURE_NAME = "myStructureOneName";
	private static final String TEMPLATE_KEY = "myTemplateKey";
	private static final String TEMPLATE_NAME = STRUCTURE_NAME + " - templateFileName";
	private static final long COMPANY_ID = 1L;
	private static final long GROUP_ID = 2L;
	private static final long USER_ID = 3L;
	private static final long JOURNAL_CLASS_NAME_ID = 5L;

	private DDMTemplateCreatorHelperImpl ddmTemplateCreatorHelperImpl;

	@Mock
	private WebContentCreatorUtil mockWebContentCreatorUtil;

	@Mock
	private InputOuputUtil mockInputOutputUtil;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalStructure;

	@Mock
	private InputStream mockInputStreamOne;

	@Mock
	private InputStream mockInputStreamTwo;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private Group mockGroup;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Before
	public void setUp() {
		initMocks(this);

		mockStatic(PortalUtil.class);

		ddmTemplateCreatorHelperImpl = new DDMTemplateCreatorHelperImpl();
		ddmTemplateCreatorHelperImpl.setWebContentCreatorUtil(mockWebContentCreatorUtil);
		ddmTemplateCreatorHelperImpl.setDdmStructureLocalStructure(mockDDMStructureLocalStructure);
		ddmTemplateCreatorHelperImpl.setInputOutputUtil(mockInputOutputUtil);
	}

	@Test
	public void createMissingWebContentTemplate_WhenNoError_ThenCallsGetOrCreateDDMTemplate() throws Exception {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(mockInputOutputUtil.getKeyFromFileName(TEMPLATE_NAME)).thenReturn(TEMPLATE_KEY);
		when(mockInputOutputUtil.getKeyFromFileName(STRUCTURE_NAME)).thenReturn(STRUCTURE_KEY);
		when(mockDDMStructureLocalStructure.fetchStructure(GROUP_ID, JOURNAL_CLASS_NAME_ID, STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		ddmTemplateCreatorHelperImpl.createMissingWebContentTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStreamOne);

		verify(mockWebContentCreatorUtil, times(1)).getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStreamOne, TEMPLATE_KEY, mockDDMStructure);
	}

	@Test
	public void createMissingWebContentTemplates_WhenNoError_ThenCreatesEachTemplate() throws Exception {
		String secondTemplateName = "secondTemplateName";
		String secondTemplateKey = "secondTemplateKey";
		Map<String, InputStream> templatesMap = new HashMap<>();
		templatesMap.put(TEMPLATE_NAME, mockInputStreamOne);
		templatesMap.put(secondTemplateName, mockInputStreamTwo);
		when(mockInputOutputUtil.getKeyFromFileName(TEMPLATE_NAME)).thenReturn(TEMPLATE_KEY);
		when(mockInputOutputUtil.getKeyFromFileName(secondTemplateName)).thenReturn(secondTemplateKey);
		when(mockDDMStructure.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMStructure.getGroupId()).thenReturn(GROUP_ID);
		when(mockDDMStructure.getUserId()).thenReturn(USER_ID);

		ddmTemplateCreatorHelperImpl.createMissingWebContentTemplates(mockDDMStructure, templatesMap);

		verify(mockWebContentCreatorUtil, times(1)).getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStreamOne, TEMPLATE_KEY, mockDDMStructure);
		verify(mockWebContentCreatorUtil, times(1)).getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, secondTemplateName, mockInputStreamTwo, secondTemplateKey, mockDDMStructure);
	}

	@Test(expected = JournalCreationException.class)
	public void createMissingWebContentTemplates_WhenExceptionCreatingAtemplate_ThenThrowsJournalCreationException() throws Exception {
		Map<String, InputStream> templatesMap = new HashMap<>();
		templatesMap.put(TEMPLATE_NAME, mockInputStreamOne);
		when(mockInputOutputUtil.getKeyFromFileName(TEMPLATE_NAME)).thenReturn(TEMPLATE_KEY);
		when(mockDDMStructure.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockDDMStructure.getGroupId()).thenReturn(GROUP_ID);
		when(mockDDMStructure.getUserId()).thenReturn(USER_ID);
		when(mockWebContentCreatorUtil.getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStreamOne, TEMPLATE_KEY, mockDDMStructure))
				.thenThrow(new JournalCreationException("msg"));

		ddmTemplateCreatorHelperImpl.createMissingWebContentTemplates(mockDDMStructure, templatesMap);
	}

	@Test
	public void getOrCreateWebContentTemplate_WhenNoError_ThenReturnsTheDDMTemplate() throws JournalCreationException {
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getCreatorUserId()).thenReturn(USER_ID);
		when(mockWebContentCreatorUtil.getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStreamOne, TEMPLATE_KEY, mockDDMStructure)).thenReturn(mockDDMTemplate);

		DDMTemplate result = ddmTemplateCreatorHelperImpl.getOrCreateWebContentTemplate(mockGroup, mockDDMStructure, TEMPLATE_NAME, TEMPLATE_KEY, mockInputStreamOne);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

	@Test
	public void getOrCreateWebContentTemplateWithNoStructure_WhenNoStructure_ThenReturnsTheDDMTemplate() throws JournalCreationException {
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getCreatorUserId()).thenReturn(USER_ID);
		when(mockWebContentCreatorUtil.getOrCreateDDMTemplateWithNoStructure(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStreamOne, TEMPLATE_KEY)).thenReturn(mockDDMTemplate);

		DDMTemplate result = ddmTemplateCreatorHelperImpl.getOrCreateWebContentTemplateWithNoStructure(mockGroup, TEMPLATE_NAME, TEMPLATE_KEY, mockInputStreamOne);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

}
