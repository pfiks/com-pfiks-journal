/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.utils;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.io.InputStream;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.mapping.model.DDMForm;
import com.liferay.dynamic.data.mapping.model.DDMFormLayout;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DDM;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.pfiks.common.exception.CommonUtilsException;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.journal.exception.JournalCreationException;

@SuppressWarnings("unchecked")
@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortalUtil.class, LocaleUtil.class })
public class WebContentCreatorUtilTest {

	private static final String TEMPLATE_NAME = "myStructureOneName - templateFileName";
	private static final String TEMPLATE_KEY = "myTemplateKey";
	private static final String STRUCTURE_KEY = "myStructureOneKey";
	private static final String STRUCTURE_NAME = "myStructureOneName";
	private static final long COMPANY_ID = 1L;
	private static final long GROUP_ID = 2L;
	private static final long USER_ID = 3L;
	private static final long STRUCTURE_ID = 4L;
	private static final long JOURNAL_CLASS_NAME_ID = 5L;
	private static final long DDMSTRUCTURE_CLASS_NAME_ID = 6L;
	private static final String INPUT_STREAM_CONTENT = "INPUT_STREAM_CONTENT [$LOCALE_DEFAULT$]";
	private static final Locale LOCALE_DEFAULT = Locale.CANADA_FRENCH;
	private static final String INPUT_STREAM_CONTENT_REPLACED = "INPUT_STREAM_CONTENT " + LOCALE_DEFAULT.toString();

	private WebContentCreatorUtil webcontentCreatorUtil;

	@Mock
	private ServiceContextHelper mockServiceContextHelper;

	@Mock
	private LocaleMapUtil mockLocaleMapUtil;

	@Mock
	private InputOuputUtil mockInputOutputUtil;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private DDM mockDDM;

	@Mock
	private DDMForm mockDDMForm;

	@Mock
	private DDMFormLayout mockDDMFormLayout;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private Map<Locale, String> mockNameMap;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(PortalUtil.class, LocaleUtil.class);

		webcontentCreatorUtil = new WebContentCreatorUtil();

		webcontentCreatorUtil.setDDMStructureLocalStructure(mockDDMStructureLocalService);
		webcontentCreatorUtil.setDDMTemplateLocalService(mockDDMTemplateLocalService);
		webcontentCreatorUtil.setInputOutputUtil(mockInputOutputUtil);
		webcontentCreatorUtil.setLocaleMapUtil(mockLocaleMapUtil);
		webcontentCreatorUtil.setDdm(mockDDM);
		webcontentCreatorUtil.setServiceContextHelper(mockServiceContextHelper);
	}

	@Test(expected = JournalCreationException.class)
	public void getOrCreateDDMStructure_WhenException_ThenThrowsJournalCreationException() throws Exception {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JOURNAL_CLASS_NAME_ID, STRUCTURE_KEY)).thenThrow(new PortalException());
		when(mockInputOutputUtil.getStringFromInputStream(mockInputStream)).thenThrow(new CommonUtilsException("msg", null));

		webcontentCreatorUtil.getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, STRUCTURE_NAME, STRUCTURE_KEY, mockInputStream);
	}

	@Test
	public void getOrCreateDDMStructure_WhenStructureAlreadyExistForTheStructureKey_ThenReturnsTheStructure() throws Exception {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JOURNAL_CLASS_NAME_ID, STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		DDMStructure result = webcontentCreatorUtil.getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, STRUCTURE_NAME, STRUCTURE_KEY, mockInputStream);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Test
	public void getOrCreateDDMStructure_WhenStructureDoesNotExistForTheStructureKey_ThenReturnsTheNewlyCreatedStructure() throws Exception {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JOURNAL_CLASS_NAME_ID, STRUCTURE_KEY)).thenThrow(new PortalException());
		when(mockInputOutputUtil.getStringFromInputStream(mockInputStream)).thenReturn(INPUT_STREAM_CONTENT);
		when(mockDDM.getDDMForm(INPUT_STREAM_CONTENT_REPLACED)).thenReturn(mockDDMForm);
		when(mockLocaleMapUtil.getLocaleMapForAvailaleLocales(STRUCTURE_NAME)).thenReturn(mockNameMap);
		when(mockDDM.getDefaultDDMFormLayout(mockDDMForm)).thenReturn(mockDDMFormLayout);
		when(mockServiceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID)).thenReturn(mockServiceContext);

		when(LocaleUtil.getSiteDefault()).thenReturn(LOCALE_DEFAULT);

		when(mockDDMStructureLocalService.addStructure(USER_ID, GROUP_ID, DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID, JOURNAL_CLASS_NAME_ID, STRUCTURE_KEY, mockNameMap, mockNameMap,
				mockDDMForm, mockDDMFormLayout, "json", DDMStructureConstants.TYPE_DEFAULT, mockServiceContext)).thenReturn(mockDDMStructure);

		DDMStructure result = webcontentCreatorUtil.getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, STRUCTURE_NAME, STRUCTURE_KEY, mockInputStream);

		assertThat(result, sameInstance(mockDDMStructure));
	}

	@Test
	public void getOrCreateDDMStructure_WhenStructureAlreadyExistForTheStructureKey_ThenDoesNotCreateNewStructure() throws Exception {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, JOURNAL_CLASS_NAME_ID, STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		webcontentCreatorUtil.getOrCreateDDMStructure(COMPANY_ID, GROUP_ID, USER_ID, STRUCTURE_NAME, STRUCTURE_KEY, mockInputStream);

		verify(mockDDMStructureLocalService, never()).addStructure(anyLong(), anyLong(), anyLong(), anyLong(), anyString(), anyMap(), anyMap(), any(DDMForm.class), any(DDMFormLayout.class),
				anyString(), anyInt(), any(ServiceContext.class));
		verifyZeroInteractions(mockLocaleMapUtil, mockServiceContextHelper);
	}

	@Test(expected = JournalCreationException.class)
	public void getOrCreateDDMTemplate_WhenStructureIsNull_ThenThrowsJournalCreationException() throws Exception {
		webcontentCreatorUtil.getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStream, TEMPLATE_KEY, null);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateAlreadyExists_ThenReturnsTheExistingTemplate() throws Exception {
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMTemplateLocalService.fetchTemplate(GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, TEMPLATE_KEY)).thenReturn(mockDDMTemplate);

		DDMTemplate result = webcontentCreatorUtil.getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStream, TEMPLATE_KEY, mockDDMStructure);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateAlreadyExists_ThenDoesNotCreateNewTemplate() throws Exception {
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMTemplateLocalService.fetchTemplate(GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, TEMPLATE_KEY)).thenReturn(mockDDMTemplate);

		webcontentCreatorUtil.getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStream, TEMPLATE_KEY, mockDDMStructure);

		verify(mockDDMTemplateLocalService, never()).addTemplate(anyLong(), anyLong(), anyLong(), anyLong(), anyLong(), anyMap(), anyMap(), anyString(), anyString(), anyString(), anyString(),
				any(ServiceContext.class));
		verifyZeroInteractions(mockServiceContextHelper, mockLocaleMapUtil);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenNoTemplateExistsWithTheSameKey_ThenReturnsTheNewlyCreatedTemplate() throws Exception {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMTemplateLocalService.fetchTemplate(GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, TEMPLATE_KEY)).thenReturn(null);
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockLocaleMapUtil.getLocaleMapForAvailaleLocales(TEMPLATE_NAME)).thenReturn(mockNameMap);
		when(mockInputOutputUtil.getStringFromInputStream(mockInputStream)).thenReturn(INPUT_STREAM_CONTENT);
		when(mockServiceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID)).thenReturn(mockServiceContext);
		when(mockDDMTemplateLocalService.addTemplate(USER_ID, GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, STRUCTURE_ID, JOURNAL_CLASS_NAME_ID, TEMPLATE_KEY, mockNameMap, mockNameMap,
				DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE, "ftl", INPUT_STREAM_CONTENT, true, false, null, null, mockServiceContext))
						.thenReturn(mockDDMTemplate);

		DDMTemplate result = webcontentCreatorUtil.getOrCreateDDMTemplate(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStream, TEMPLATE_KEY, mockDDMStructure);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

	@Test(expected = JournalCreationException.class)
	public void getOrCreateDDMTemplateWithNoStructure_WhenErrorCreatingTemplate_ThenThrowsJournalCreationException() throws Exception {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMTemplateLocalService.fetchTemplate(GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, TEMPLATE_KEY)).thenReturn(null);
		when(mockLocaleMapUtil.getLocaleMapForAvailaleLocales(TEMPLATE_NAME)).thenReturn(mockNameMap);
		when(mockInputOutputUtil.getStringFromInputStream(mockInputStream)).thenReturn(INPUT_STREAM_CONTENT);
		when(mockServiceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID)).thenReturn(mockServiceContext);
		when(mockDDMTemplateLocalService.addTemplate(USER_ID, GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, 0, JOURNAL_CLASS_NAME_ID, TEMPLATE_KEY, mockNameMap, mockNameMap,
				DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE, "ftl", INPUT_STREAM_CONTENT, true, false, null, null, mockServiceContext))
						.thenThrow(new PortalException());

		webcontentCreatorUtil.getOrCreateDDMTemplateWithNoStructure(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStream, TEMPLATE_KEY);
	}

	@Test
	public void getOrCreateDDMTemplateWithNoStructure_WhenTemplateAlreadyExists_ThenReturnsTheExistingTemplate() throws Exception {
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMTemplateLocalService.fetchTemplate(GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, TEMPLATE_KEY)).thenReturn(mockDDMTemplate);

		DDMTemplate result = webcontentCreatorUtil.getOrCreateDDMTemplateWithNoStructure(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStream, TEMPLATE_KEY);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

	@Test
	public void getOrCreateDDMTemplateWithNoStructure_WhenTemplateAlreadyExists_ThenDoesNotCreateNewTemplate() throws Exception {
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMTemplateLocalService.fetchTemplate(GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, TEMPLATE_KEY)).thenReturn(mockDDMTemplate);

		webcontentCreatorUtil.getOrCreateDDMTemplateWithNoStructure(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStream, TEMPLATE_KEY);

		verify(mockDDMTemplateLocalService, never()).addTemplate(anyLong(), anyLong(), anyLong(), anyLong(), anyLong(), anyMap(), anyMap(), anyString(), anyString(), anyString(), anyString(),
				any(ServiceContext.class));
		verifyZeroInteractions(mockServiceContextHelper, mockLocaleMapUtil);
	}

	@Test
	public void getOrCreateDDMTemplateWithNoStructure_WhenNoTemplateExistsWithTheSameKey_ThenReturnsTheNewlyCreatedTemplate() throws Exception {
		when(PortalUtil.getClassNameId(JournalArticle.class)).thenReturn(JOURNAL_CLASS_NAME_ID);
		when(PortalUtil.getClassNameId(DDMStructure.class)).thenReturn(DDMSTRUCTURE_CLASS_NAME_ID);
		when(mockDDMTemplateLocalService.fetchTemplate(GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, TEMPLATE_KEY)).thenReturn(null);
		when(mockLocaleMapUtil.getLocaleMapForAvailaleLocales(TEMPLATE_NAME)).thenReturn(mockNameMap);
		when(mockInputOutputUtil.getStringFromInputStream(mockInputStream)).thenReturn(INPUT_STREAM_CONTENT);
		when(mockServiceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID)).thenReturn(mockServiceContext);
		when(mockDDMTemplateLocalService.addTemplate(USER_ID, GROUP_ID, DDMSTRUCTURE_CLASS_NAME_ID, 0, JOURNAL_CLASS_NAME_ID, TEMPLATE_KEY, mockNameMap, mockNameMap,
				DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY, DDMTemplateConstants.TEMPLATE_MODE_CREATE, "ftl", INPUT_STREAM_CONTENT, true, false, null, null, mockServiceContext))
						.thenReturn(mockDDMTemplate);

		DDMTemplate result = webcontentCreatorUtil.getOrCreateDDMTemplateWithNoStructure(COMPANY_ID, GROUP_ID, USER_ID, TEMPLATE_NAME, mockInputStream, TEMPLATE_KEY);

		assertThat(result, sameInstance(mockDDMTemplate));
	}

}
