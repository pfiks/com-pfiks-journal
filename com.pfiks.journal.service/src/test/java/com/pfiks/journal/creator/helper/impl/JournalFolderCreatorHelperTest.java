/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalFolderLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.journal.creator.helper.impl.JournalFolderCreatorHelperImpl;

public class JournalFolderCreatorHelperTest {

	private JournalFolderCreatorHelperImpl journalFolderCreatorHelperImpl = new JournalFolderCreatorHelperImpl();

	private static final String FOLDER_NAME = "myFolderName";
	private static final String FOLDER_DESC = "myFolderDesc";
	private static final long COMPANY_ID = 1L;
	private static final long GROUP_ID = 2L;
	private static final long USER_ID = 3L;

	@Mock
	private ServiceContextHelper mockServiceContextHelper;

	@Mock
	private JournalFolderLocalService mockJournalFolderLocalService;

	@Mock
	private UserLocalService mockUserLocalService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		journalFolderCreatorHelperImpl.setJournalFolderLocalService(mockJournalFolderLocalService);
		journalFolderCreatorHelperImpl.setServiceContextHelper(mockServiceContextHelper);
		journalFolderCreatorHelperImpl.setUserLocalService(mockUserLocalService);
	}

	@Test
	public void getOrCreateJournalFolder_WhenFolderNameIsEmpty_ThenNothingIsExecuted() throws Exception {
		journalFolderCreatorHelperImpl.getOrCreateJournalFolder(COMPANY_ID, GROUP_ID, null, FOLDER_DESC);

		verify(mockJournalFolderLocalService, never()).fetchFolder(anyLong(), anyString());
	}

	@Test
	public void getOrCreateJournalFolder_WhenFolderAlreadyExistsWithSameName_ThenNoFolderIsCreated_AndTheExistingFolderIsReturned() throws Exception {
		when(mockJournalFolderLocalService.fetchFolder(GROUP_ID, FOLDER_NAME)).thenReturn(mockJournalFolder);

		JournalFolder journalFolder = journalFolderCreatorHelperImpl.getOrCreateJournalFolder(COMPANY_ID, GROUP_ID, FOLDER_NAME, FOLDER_DESC);

		assertThat(journalFolder, equalTo(mockJournalFolder));

		verify(mockJournalFolderLocalService, never()).addFolder(anyLong(), anyLong(), anyLong(), anyString(), anyString(), any(ServiceContext.class));

		verifyZeroInteractions(mockUserLocalService, mockServiceContextHelper);
	}

	@Test
	public void getOrCreateJournalFolder_WhenFolderDoesNotExist_ThenItIsCreated() throws Exception {
		when(mockJournalFolderLocalService.fetchFolder(GROUP_ID, FOLDER_NAME)).thenReturn(null);
		when(mockUserLocalService.getDefaultUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockServiceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID)).thenReturn(mockServiceContext);

		when(mockJournalFolderLocalService.addFolder(USER_ID, GROUP_ID, 0, FOLDER_NAME, FOLDER_DESC, mockServiceContext)).thenReturn(mockJournalFolder);

		JournalFolder journalFolder = journalFolderCreatorHelperImpl.getOrCreateJournalFolder(COMPANY_ID, GROUP_ID, FOLDER_NAME, FOLDER_DESC);

		assertThat(journalFolder, equalTo(mockJournalFolder));

		verify(mockJournalFolderLocalService, times(1)).addFolder(USER_ID, GROUP_ID, 0, FOLDER_NAME, FOLDER_DESC, mockServiceContext);
	}

}
