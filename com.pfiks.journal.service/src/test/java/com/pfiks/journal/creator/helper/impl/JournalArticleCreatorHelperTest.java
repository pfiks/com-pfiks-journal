/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.pfiks.common.exception.CommonUtilsException;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.journal.constants.WebContentStructure;
import com.pfiks.journal.creator.helper.DDMStructureCreatorHelper;
import com.pfiks.journal.creator.helper.JournalFolderCreatorHelper;
import com.pfiks.journal.creator.helper.utils.WebContentUtil;
import com.pfiks.journal.creator.helper.utils.constants.CreationConstants;
import com.pfiks.journal.exception.JournalCreationException;

public class JournalArticleCreatorHelperTest {

	private static final long GROUP_ID = 1L;
	private static final long COMPANY_ID = 2L;
	private static final Long GLOBAL_GROUP_ID = 3l;
	private static final long CLASS_NAME_ID = 4l;
	private static final String JOURNAL_ARTICLE_FILE_NAME_ONE = "myJournalArticleFileNameOne";
	private static final String JOURNAL_ARTICLE_ID_ONE = "JOURNAL_ARTICLE_ID_ONE";
	private static final String JOURNAL_CONTENT_ONE = "JOURNAL_CONTENT_ONE";
	private static final String JOURNAL_ARTICLE_FILE_NAME_TWO = "myJournalArticleFileNameTwo";
	private static final String JOURNAL_ARTICLE_ID_TWO = "JOURNAL_ARTICLE_ID_TWO";
	private static final String JOURNAL_CONTENT_TWO = "JOURNAL_CONTENT_TWO";
	private static final String FOLDER_NAME = "folderName";
	private static final String TEMPLATE_KEY = "templateKey";
	private static final String STRUCTURE_KEY = "structureKey";

	private JournalArticleCreatorHelperImpl journalArticleCreatorHelperImpl;

	@Mock
	private DDMStructureCreatorHelper mockDDMStructureCreatorHelper;

	@Mock
	private InputOuputUtil mockInputOutputUtil;

	@Mock
	private WebContentUtil mockWebContentUtil;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructure mockDDMStructureChecked;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private InputStream mockInputStreamOne;

	@Mock
	private InputStream mockInputStreamTwo;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Group mockGroup;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private JournalFolderCreatorHelper mockJournalFolderCreatorHelper;

	@Mock
	private GroupLocalService mockGroupLocalService;

	private Map<String, InputStream> inputStreams;

	@Before
	public void setUp() {
		initMocks(this);

		journalArticleCreatorHelperImpl = new JournalArticleCreatorHelperImpl();
		journalArticleCreatorHelperImpl.setDDMStructureLocalService(mockDDMStructureLocalService);
		journalArticleCreatorHelperImpl.setDDMStructureCreatorHelper(mockDDMStructureCreatorHelper);
		journalArticleCreatorHelperImpl.setJournalFolderCreatorHelper(mockJournalFolderCreatorHelper);
		journalArticleCreatorHelperImpl.setWebContentUtil(mockWebContentUtil);
		journalArticleCreatorHelperImpl.setInputOutputUtil(mockInputOutputUtil);
		journalArticleCreatorHelperImpl.setGroupLocalService(mockGroupLocalService);

		inputStreams = new HashMap<>();
	}

	@Test(expected = JournalCreationException.class)
	public void createMissingJournalArticle_WithGroupIdAndCompanyIdParams_WhenException_ThenThrowsJournalCreationException() throws Exception {
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenThrow(new PortalException());

		journalArticleCreatorHelperImpl.createMissingJournalArticle(COMPANY_ID, GROUP_ID, mockDDMStructure, mockJournalFolder, true, JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);
	}

	@Test
	public void createMissingJournalArticle_WithGroupIdAndCompanyIdParams_WhenNoError_ThenCreatesArticle() throws Exception {
		boolean skipWorkflow = true;
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);

		journalArticleCreatorHelperImpl.createMissingJournalArticle(COMPANY_ID, GROUP_ID, mockDDMStructure, mockJournalFolder, skipWorkflow, JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, skipWorkflow);
	}

	@Test
	public void createMissingJournalArticle_WithGroupIdAndCompanyIdParams_WhenTypeSettingsDoesNotHaveLanguageIdAndAvailableLanguages_ThenCreatesArticle() throws Exception {
		boolean skipWorkflow = true;
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);

		journalArticleCreatorHelperImpl.createMissingJournalArticle(COMPANY_ID, GROUP_ID, mockDDMStructure, mockJournalFolder, skipWorkflow, JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, skipWorkflow);
	}

	@Test
	public void createMissingJournalArticle_WithGroupIdAndCompanyIdParams_WhenTypeSettingsDoesHaveLanguageIdAndAvailableLanguages_ThenCreatesArticle() throws Exception {
		boolean skipWorkflow = true;
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);

		journalArticleCreatorHelperImpl.createMissingJournalArticle(COMPANY_ID, GROUP_ID, mockDDMStructure, mockJournalFolder, skipWorkflow, JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, skipWorkflow);
	}

	@Test
	public void getOrCreateJournalArticle_WhenNoError_ThenReturnsTheCreatedArticle() throws Exception {
		boolean skipWorkflow = true;
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWebContentUtil.getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder, mockDDMStructureChecked,
				TEMPLATE_KEY, skipWorkflow)).thenReturn(mockJournalArticle);

		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);

		JournalArticle result = journalArticleCreatorHelperImpl.getOrCreateJournalArticle(GROUP_ID, mockDDMStructure, mockJournalFolder, skipWorkflow, JOURNAL_ARTICLE_FILE_NAME_ONE,
				mockInputStreamOne);

		assertThat(result, sameInstance(mockJournalArticle));
	}

	@Test(expected = JournalCreationException.class)
	public void getOrCreateJournalArticle_WhenException_ThenThrowsJournalCreationException() throws Exception {
		journalArticleCreatorHelperImpl.getOrCreateJournalArticle(GROUP_ID, mockDDMStructure, mockJournalFolder, true, JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);
	}

	@Test(expected = JournalCreationException.class)
	public void createMissingJournalArticle_WithInputStreamParam_WhenException_ThenThrowsJournalCreationException() throws Exception {
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenThrow(new CommonUtilsException("msg", null));

		journalArticleCreatorHelperImpl.createMissingJournalArticle(mockGroup, mockDDMStructure, mockJournalFolder, true, JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);
	}

	@Test
	public void createMissingJournalArticle_WithInputStreamParam_WhenNoError_ThenCreatesArticle() throws Exception {
		boolean skipWorkflow = true;
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);

		journalArticleCreatorHelperImpl.createMissingJournalArticle(mockGroup, mockDDMStructure, mockJournalFolder, skipWorkflow, JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, skipWorkflow);
	}

	@Test(expected = JournalCreationException.class)
	public void createMissingJournalArticle_WithStringParams_WhenException_ThenThrowsJournalCreationException() throws Exception {
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenThrow(new PortalException());

		journalArticleCreatorHelperImpl.createMissingJournalArticle(COMPANY_ID, GROUP_ID, mockDDMStructure, mockJournalFolder, true, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE,
				JOURNAL_CONTENT_ONE);
	}

	@Test
	public void createMissingJournalArticle_WithStringParams_WhenNoError_ThenCreatesArticle() throws Exception {
		boolean skipWorkflow = true;
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);

		journalArticleCreatorHelperImpl.createMissingJournalArticle(COMPANY_ID, GROUP_ID, mockDDMStructure, mockJournalFolder, skipWorkflow, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE,
				JOURNAL_CONTENT_ONE);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, skipWorkflow);
	}

	@Test
	public void createMissingJournalArticle_WithInputStream_WhenTypeSettingsHasLanguageIdAndAvailableLanguages_ThenCreatesArticle() throws Exception {
		boolean skipWorkflow = true;
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);

		journalArticleCreatorHelperImpl.createMissingJournalArticle(mockGroup, mockDDMStructure, mockJournalFolder, skipWorkflow, JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, skipWorkflow);
	}

	@Test(expected = JournalCreationException.class)
	public void createMissingJournalArticles_WithStructureKeyParam_WhenException_ThenThrowsJournalCreationException() throws Exception {
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);

		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenThrow(new PortalException());

		journalArticleCreatorHelperImpl.createMissingJournalArticles(COMPANY_ID, GROUP_ID, STRUCTURE_KEY, FOLDER_NAME, inputStreams);
	}

	@Test
	public void createMissingJournalArticles_WithStructureKeyParam_WhenArticlesToCreateMapIsEmpty_ThenNoActionIsPerformed() throws Exception {
		journalArticleCreatorHelperImpl.createMissingJournalArticles(COMPANY_ID, GROUP_ID, STRUCTURE_KEY, FOLDER_NAME, inputStreams);

		verifyZeroInteractions(mockWebContentUtil);
	}

	@Test
	public void createMissingJournalArticles_WithStructureKeyParam_WhenStructureIsFound_ThenCreatesEachArticle() throws Exception {
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_TWO, mockInputStreamTwo);

		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getJournalClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockJournalFolderCreatorHelper.getOrCreateJournalFolder(COMPANY_ID, GROUP_ID, FOLDER_NAME, StringPool.BLANK)).thenReturn(mockJournalFolder);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockDDMStructureLocalService.fetchStructure(GLOBAL_GROUP_ID, CLASS_NAME_ID, STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_TWO)).thenReturn(JOURNAL_ARTICLE_ID_TWO);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamTwo)).thenReturn(JOURNAL_CONTENT_TWO);

		journalArticleCreatorHelperImpl.createMissingJournalArticles(COMPANY_ID, GROUP_ID, STRUCTURE_KEY, FOLDER_NAME, inputStreams);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, true);
		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_TWO, JOURNAL_ARTICLE_FILE_NAME_TWO, JOURNAL_CONTENT_TWO, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, true);
	}

	@Test
	public void createMissingJournalArticles_WithStructureKeyParam_WhenStructureIsNotFoundAndStructureKeyIsNotEmail_ThenCreatesEachArticleWithTheCheckedStructure() throws Exception {
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_TWO, mockInputStreamTwo);

		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getJournalClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockJournalFolderCreatorHelper.getOrCreateJournalFolder(COMPANY_ID, GROUP_ID, FOLDER_NAME, StringPool.BLANK)).thenReturn(mockJournalFolder);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockDDMStructureLocalService.fetchStructure(GLOBAL_GROUP_ID, CLASS_NAME_ID, STRUCTURE_KEY)).thenReturn(null);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, null)).thenReturn(mockDDMStructureChecked);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_TWO)).thenReturn(JOURNAL_ARTICLE_ID_TWO);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamTwo)).thenReturn(JOURNAL_CONTENT_TWO);

		journalArticleCreatorHelperImpl.createMissingJournalArticles(COMPANY_ID, GROUP_ID, STRUCTURE_KEY, FOLDER_NAME, inputStreams);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, true);
		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_TWO, JOURNAL_ARTICLE_FILE_NAME_TWO, JOURNAL_CONTENT_TWO, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, true);
	}

	@Test
	public void createMissingJournalArticles_WithStructureKeyParam_WhenStructureIsNotFoundAndStructureKeyIsEmail_ThenCreatesEachArticleAfterCreatingTheEmailStructure() throws Exception {
		String structureKeyEmail = WebContentStructure.EMAIL.getValue();
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_TWO, mockInputStreamTwo);

		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getJournalClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockJournalFolderCreatorHelper.getOrCreateJournalFolder(COMPANY_ID, GROUP_ID, FOLDER_NAME, StringPool.BLANK)).thenReturn(mockJournalFolder);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockDDMStructureLocalService.getStructure(GLOBAL_GROUP_ID, CLASS_NAME_ID, structureKeyEmail)).thenReturn(null);
		when(mockDDMStructureCreatorHelper.getOrCreateWebContentStructure(COMPANY_ID, WebContentStructure.EMAIL)).thenReturn(mockDDMStructure);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_TWO)).thenReturn(JOURNAL_ARTICLE_ID_TWO);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamTwo)).thenReturn(JOURNAL_CONTENT_TWO);

		journalArticleCreatorHelperImpl.createMissingJournalArticles(COMPANY_ID, GROUP_ID, structureKeyEmail, FOLDER_NAME, inputStreams);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, true);
		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_TWO, JOURNAL_ARTICLE_FILE_NAME_TWO, JOURNAL_CONTENT_TWO, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, true);
	}

	@Test(expected = JournalCreationException.class)
	public void createMissingJournalArticles_WithoutStructureKeyParam_WhenException_ThenThrowsJournalCreationException() throws Exception {
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);

		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenThrow(new PortalException());

		journalArticleCreatorHelperImpl.createMissingJournalArticles(COMPANY_ID, GROUP_ID, FOLDER_NAME, inputStreams);
	}

	@Test
	public void createMissingJournalArticles_WithoutStructureKeyParam_WhenArticlesToCreateMapIsEmpty_ThenNoActionIsPerformed() throws Exception {
		journalArticleCreatorHelperImpl.createMissingJournalArticles(COMPANY_ID, GROUP_ID, FOLDER_NAME, inputStreams);

		verifyZeroInteractions(mockWebContentUtil);
	}

	@Test
	public void createMissingJournalArticles_WithoutStructureKeyParam_WhenNoError_ThenCreatesEachArticleWithBasicWebContentStructure() throws Exception {
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_ONE, mockInputStreamOne);
		inputStreams.put(JOURNAL_ARTICLE_FILE_NAME_TWO, mockInputStreamTwo);

		when(mockWebContentUtil.getServiceContext(COMPANY_ID, GROUP_ID)).thenReturn(mockServiceContext);
		when(mockWebContentUtil.getJournalClassNameId()).thenReturn(CLASS_NAME_ID);
		when(mockJournalFolderCreatorHelper.getOrCreateJournalFolder(COMPANY_ID, GROUP_ID, FOLDER_NAME, StringPool.BLANK)).thenReturn(mockJournalFolder);
		when(mockWebContentUtil.getGlobalGroupId(COMPANY_ID)).thenReturn(GLOBAL_GROUP_ID);
		when(mockDDMStructureLocalService.fetchStructure(GLOBAL_GROUP_ID, CLASS_NAME_ID, CreationConstants.BASIC_WEB_CONTENT_STRUCTURE_KEY)).thenReturn(mockDDMStructure);
		when(mockWebContentUtil.getDefaultTemplateKeyForStructure(mockDDMStructureChecked)).thenReturn(TEMPLATE_KEY);
		when(mockWebContentUtil.checkWebContentStructure(mockServiceContext, GLOBAL_GROUP_ID, mockDDMStructure)).thenReturn(mockDDMStructureChecked);
		when(mockGroupLocalService.getGroup(GROUP_ID)).thenReturn(mockGroup);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_ONE)).thenReturn(JOURNAL_ARTICLE_ID_ONE);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamOne)).thenReturn(JOURNAL_CONTENT_ONE);
		when(mockInputOutputUtil.getKeyFromFileName(JOURNAL_ARTICLE_FILE_NAME_TWO)).thenReturn(JOURNAL_ARTICLE_ID_TWO);
		when(mockInputOutputUtil.getLocalizedContentFromInputStream(mockGroup, mockInputStreamTwo)).thenReturn(JOURNAL_CONTENT_TWO);

		journalArticleCreatorHelperImpl.createMissingJournalArticles(COMPANY_ID, GROUP_ID, FOLDER_NAME, inputStreams);

		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_ONE, JOURNAL_ARTICLE_FILE_NAME_ONE, JOURNAL_CONTENT_ONE, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, true);
		verify(mockWebContentUtil, times(1)).getOrCreateJournalArticle(mockServiceContext, JOURNAL_ARTICLE_ID_TWO, JOURNAL_ARTICLE_FILE_NAME_TWO, JOURNAL_CONTENT_TWO, mockJournalFolder,
				mockDDMStructureChecked, TEMPLATE_KEY, true);
	}

}
