/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.service.impl;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.Reader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.pfiks.journal.exception.JournalProcessingException;
import com.pfiks.journal.model.JournalArticleContent;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SAXReaderUtil.class, PropsUtil.class, PortalUtil.class, LanguageUtil.class })
public class JournalArticleProcessorImplTest {

	private static final String FIELD_NAME = "fieldName";
	private static final String FIELD_TEXT = "Field_Text";
	private static final String XML_ARTICLE_CONTENT = "XML_ARTICLE_CONTENT";
	private static final long GROUP_ID = 1;
	private static final String ARTICLE_ID = "articleId";
	private static final String ARTICLE_CONTENT = "JournalArticle content";
	private static final String GROUP_LANGUAGE_ID = "siteLanguageId";

	private JournalArticleProcessorImpl journalArticleProcessorImpl;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private Document mockDocument;

	@Mock
	private Node mockNode;

	@Mock
	private JournalArticle mockJournalArticle;

	private Map<String, String> values;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(PropsUtil.class, PortalUtil.class, SAXReaderUtil.class, LanguageUtil.class);

		values = new HashMap<>();
		journalArticleProcessorImpl = new JournalArticleProcessorImpl();
		journalArticleProcessorImpl.setJournalArticleLocalService(mockJournalArticleLocalService);
	}

	@Test(expected = JournalProcessingException.class)
	public void getFieldValue_WhenError_ThenThrowsJournalProcessingException() throws Exception {
		when(SAXReaderUtil.read(any(Reader.class))).thenThrow(new DocumentException());

		journalArticleProcessorImpl.getFieldValue(FIELD_NAME, XML_ARTICLE_CONTENT);
	}

	@Test
	public void getFieldValue_WithFieldNameAndFieldValue_ThenReturnTheFieldValue() throws Exception {
		String fieldName = FIELD_NAME;
		String expectedValue = FIELD_TEXT;
		mockJournalArticleXMLDocument(fieldName, expectedValue);

		String result = journalArticleProcessorImpl.getFieldValue(fieldName, XML_ARTICLE_CONTENT);

		assertThat(result, equalTo(expectedValue));
	}

	@Test(expected = JournalProcessingException.class)
	public void getFieldValue_WhenFieldNameIsNotFound_ThenThrows() throws Exception {
		String fieldName = FIELD_NAME;
		when(SAXReaderUtil.read(any(Reader.class))).thenReturn(mockDocument);
		when(mockDocument.selectSingleNode("/root/dynamic-element[@name='" + fieldName + "']/dynamic-content")).thenReturn(null);

		journalArticleProcessorImpl.getFieldValue(fieldName, XML_ARTICLE_CONTENT);
	}

	@Test
	public void getFieldValue_WhenFieldNameIsFoundButHasEmptyValue_ThenReturnsEmptyString() throws Exception {
		String fieldName = FIELD_NAME;
		mockJournalArticleXMLDocument(fieldName, "");

		String result = journalArticleProcessorImpl.getFieldValue(fieldName, XML_ARTICLE_CONTENT);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test(expected = JournalProcessingException.class)
	public void getPopulatedContent_WhenError_ThenThrowsJournalProcessingException() throws Exception {
		when(mockJournalArticleLocalService.getArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());

		journalArticleProcessorImpl.getPopulatedContent(GROUP_ID, ARTICLE_ID, values);
	}

	@Test
	public void getPopulatedContent_WithArticleContentAndPlaceholdersParameters_WhenNoError_ThenReturnsTheContentWithTheReplacedValues() throws Exception {
		String keyOne = "myKeyOne";
		String valueOne = "myValueOne";
		String keyTwo = "myKeyTwo";
		String valueTwo = "myValueTwo";

		String content = "test " + keyOne + ", " + keyTwo;
		String expectedContent = "test " + valueOne + ", " + valueTwo;

		values.put(keyOne, valueOne);
		values.put("UnmatchedKey", "UnmatchedValue");
		values.put(keyTwo, valueTwo);

		String result = journalArticleProcessorImpl.getPopulatedContent(content, values);

		assertThat(result, equalTo(expectedContent));
	}

	@Test
	public void getPopulatedContent_WhenJournalArticleIsFound_ThenReturnsItsContentWithTheReplacedValues() throws Exception {
		String key = "[$NAME$]";
		String value = "New Name";
		String content = ARTICLE_CONTENT + key;
		String expectedContent = ARTICLE_CONTENT + value;

		values.put(key, value);
		values.put("UnmatchedKey", "UnmatchedValue");

		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockJournalArticle.getContentByLocale(GROUP_LANGUAGE_ID)).thenReturn(content);
		mockGroupLanguageId();

		String result = journalArticleProcessorImpl.getPopulatedContent(GROUP_ID, ARTICLE_ID, values);

		assertThat(result, equalTo(expectedContent));
	}

	@Test
	public void getPopulatedContent_WhenJournalArticleIsFoundAndHasNoContent_ThenReturnsEmptyString() throws Exception {
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockJournalArticle.getContentByLocale(GROUP_LANGUAGE_ID)).thenReturn(StringPool.BLANK);
		mockGroupLanguageId();

		String result = journalArticleProcessorImpl.getPopulatedContent(GROUP_ID, ARTICLE_ID, values);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test(expected = JournalProcessingException.class)
	public void getArticleContent_WithGroupIdAndArticleIdParameters_WhenException_ThenThrowsJournalProcessingException() throws Exception {
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());

		journalArticleProcessorImpl.getArticleContent(GROUP_ID, ARTICLE_ID);
	}

	@Test
	public void getArticleContent_WithGroupIdAndArticleIdParameters_WhenNoError_ThenReturnsTheArticleContentInTheDefaultGroupLocale() throws Exception {
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenReturn(mockJournalArticle);
		mockGroupLanguageId();
		when(mockJournalArticle.getContentByLocale(GROUP_LANGUAGE_ID)).thenReturn(ARTICLE_CONTENT);

		String result = journalArticleProcessorImpl.getArticleContent(GROUP_ID, ARTICLE_ID);

		assertThat(result, equalTo(ARTICLE_CONTENT));
	}

	@Test(expected = JournalProcessingException.class)
	public void getArticleContent_WithGroupIdAndArticleIdAndLanguageIdParameterParameters_WhenException_ThenThrowsJournalProcessingException() throws Exception {
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());

		journalArticleProcessorImpl.getArticleContent(GROUP_ID, ARTICLE_ID, "languageId");
	}

	@Test
	public void getArticleContent_WithGroupIdAndArticleIdAndLanguageIdParameters_WhenNoError_ThenReturnsTheArticleContentInTheSpecifiedLanguage() throws Exception {
		String languageId = "customLanguageId";
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockJournalArticle.getContentByLocale(languageId)).thenReturn(ARTICLE_CONTENT);

		String result = journalArticleProcessorImpl.getArticleContent(GROUP_ID, ARTICLE_ID, languageId);

		assertThat(result, equalTo(ARTICLE_CONTENT));
	}

	@Test(expected = JournalProcessingException.class)
	public void getArticleContentByLanguageId_WithGroupIdAndArticleIdParams_WhenException_ThenThrowsJournalProcessingException() throws Exception {
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());

		journalArticleProcessorImpl.getArticleContentByLanguageId(GROUP_ID, ARTICLE_ID);
	}

	@Test
	public void getArticleContentByLanguageId_WithGroupIdAndArticleIdParams_WhenDefaultLanguageIdIsIncluded_ThenReturnsMapWithContentLocalizedForEachAvailableLocaleForTheArticle() throws Exception {
		String languageIdDefault = "languageIdOne";
		String languageIdTwo = "languageIdTwo";
		String languageIdThree = "languageIdThree";
		String contentLanguageIdDefault = "contentLanguageIdOne";
		String contentLanguageIdTwo = "contentLanguageIdTwo";
		String contentLanguageIdThree = "contentLanguageIdThree";
		String[] availableLanguageIds = new String[] { languageIdDefault, languageIdTwo, languageIdThree };

		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockJournalArticle.getDefaultLanguageId()).thenReturn(languageIdDefault);
		when(mockJournalArticle.getAvailableLanguageIds()).thenReturn(availableLanguageIds);
		when(mockJournalArticle.getContentByLocale(languageIdDefault)).thenReturn(contentLanguageIdDefault);
		when(mockJournalArticle.getContentByLocale(languageIdTwo)).thenReturn(contentLanguageIdTwo);
		when(mockJournalArticle.getContentByLocale(languageIdThree)).thenReturn(contentLanguageIdThree);

		Map<String, String> results = journalArticleProcessorImpl.getArticleContentByLanguageId(GROUP_ID, ARTICLE_ID);

		assertThat(results.size(), equalTo(3));
		assertThat(results.get(languageIdDefault), equalTo(contentLanguageIdDefault));
		assertThat(results.get(languageIdTwo), equalTo(contentLanguageIdTwo));
		assertThat(results.get(languageIdThree), equalTo(contentLanguageIdThree));
	}

	@Test
	public void getArticleContentByLanguageId_WithGroupIdAndArticleIdParams_WhenDefaultLanguageIdIsNotIncluded_ThenReturnsMapWithContentLocalizedForEachAvailableLocaleAndTheDefaultOneForTheArticle()
			throws Exception {
		String languageIdDefault = "languageIdOne";
		String languageIdTwo = "languageIdTwo";
		String languageIdThree = "languageIdThree";
		String contentLanguageIdDefault = "contentLanguageIdOne";
		String contentLanguageIdTwo = "contentLanguageIdTwo";
		String contentLanguageIdThree = "contentLanguageIdThree";
		String[] availableLanguageIds = new String[] { languageIdTwo, languageIdThree };

		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenReturn(mockJournalArticle);
		when(mockJournalArticle.getDefaultLanguageId()).thenReturn(languageIdDefault);
		when(mockJournalArticle.getAvailableLanguageIds()).thenReturn(availableLanguageIds);
		when(mockJournalArticle.getContentByLocale(languageIdDefault)).thenReturn(contentLanguageIdDefault);
		when(mockJournalArticle.getContentByLocale(languageIdTwo)).thenReturn(contentLanguageIdTwo);
		when(mockJournalArticle.getContentByLocale(languageIdThree)).thenReturn(contentLanguageIdThree);

		Map<String, String> results = journalArticleProcessorImpl.getArticleContentByLanguageId(GROUP_ID, ARTICLE_ID);

		assertThat(results.size(), equalTo(3));
		assertThat(results.get(languageIdDefault), equalTo(contentLanguageIdDefault));
		assertThat(results.get(languageIdTwo), equalTo(contentLanguageIdTwo));
		assertThat(results.get(languageIdThree), equalTo(contentLanguageIdThree));
	}

	@Test
	public void getArticleContentByLanguageId_WithJournalArticleParam_WhenDefaultLanguageIdIsIncluded_ThenReturnsMapWithContentLocalizedForEachAvailableLocaleForTheArticle() throws Exception {
		String languageIdDefault = "languageIdOne";
		String languageIdTwo = "languageIdTwo";
		String languageIdThree = "languageIdThree";
		String contentLanguageIdDefault = "contentLanguageIdOne";
		String contentLanguageIdTwo = "contentLanguageIdTwo";
		String contentLanguageIdThree = "contentLanguageIdThree";
		String[] availableLanguageIds = new String[] { languageIdDefault, languageIdTwo, languageIdThree };

		when(mockJournalArticle.getDefaultLanguageId()).thenReturn(languageIdDefault);
		when(mockJournalArticle.getAvailableLanguageIds()).thenReturn(availableLanguageIds);
		when(mockJournalArticle.getContentByLocale(languageIdDefault)).thenReturn(contentLanguageIdDefault);
		when(mockJournalArticle.getContentByLocale(languageIdTwo)).thenReturn(contentLanguageIdTwo);
		when(mockJournalArticle.getContentByLocale(languageIdThree)).thenReturn(contentLanguageIdThree);

		Map<String, String> results = journalArticleProcessorImpl.getArticleContentByLanguageId(mockJournalArticle);

		assertThat(results.size(), equalTo(3));
		assertThat(results.get(languageIdDefault), equalTo(contentLanguageIdDefault));
		assertThat(results.get(languageIdTwo), equalTo(contentLanguageIdTwo));
		assertThat(results.get(languageIdThree), equalTo(contentLanguageIdThree));
	}

	@Test
	public void getArticleContentByLanguageId_WithJournalArticleParam_WhenDefaultLanguageIdIsNotIncluded_ThenReturnsMapWithContentLocalizedForEachAvailableLocaleAndTheDefaultOneForTheArticle()
			throws Exception {
		String languageIdDefault = "languageIdOne";
		String languageIdTwo = "languageIdTwo";
		String languageIdThree = "languageIdThree";
		String contentLanguageIdDefault = "contentLanguageIdOne";
		String contentLanguageIdTwo = "contentLanguageIdTwo";
		String contentLanguageIdThree = "contentLanguageIdThree";
		String[] availableLanguageIds = new String[] { languageIdTwo, languageIdThree };

		when(mockJournalArticle.getDefaultLanguageId()).thenReturn(languageIdDefault);
		when(mockJournalArticle.getAvailableLanguageIds()).thenReturn(availableLanguageIds);
		when(mockJournalArticle.getContentByLocale(languageIdDefault)).thenReturn(contentLanguageIdDefault);
		when(mockJournalArticle.getContentByLocale(languageIdTwo)).thenReturn(contentLanguageIdTwo);
		when(mockJournalArticle.getContentByLocale(languageIdThree)).thenReturn(contentLanguageIdThree);

		Map<String, String> results = journalArticleProcessorImpl.getArticleContentByLanguageId(mockJournalArticle);

		assertThat(results.size(), equalTo(3));
		assertThat(results.get(languageIdDefault), equalTo(contentLanguageIdDefault));
		assertThat(results.get(languageIdTwo), equalTo(contentLanguageIdTwo));
		assertThat(results.get(languageIdThree), equalTo(contentLanguageIdThree));
	}

	@Test(expected = JournalProcessingException.class)
	public void getJournalArticleContent_WithLanguageIdParam_WhenException_ThenThrowsJournalProcessingException() throws Exception {
		journalArticleProcessorImpl.getJournalArticleContent(null, "languageId");
	}

	@Test
	public void getJournalArticleContent_WithLanguageIdParam_WhenNoError_ThenReturnsJournalArticleContentForTheArticleContentByLocale() throws Exception {
		String languageId = "languageId";
		String xmlContent = "<root><dynamic-element></dynamic-element></root>";
		when(mockJournalArticle.getContentByLocale(languageId)).thenReturn(xmlContent);

		JournalArticleContent result = journalArticleProcessorImpl.getJournalArticleContent(mockJournalArticle, languageId);

		assertThat(result.getDynamicElement().size(), equalTo(1));
	}

	@Test(expected = JournalProcessingException.class)
	public void getJournalArticleContent_WhenException_ThenThrowsJournalProcessingException() throws Exception {
		journalArticleProcessorImpl.getJournalArticleContent(null);
	}

	@Test
	public void getJournalArticleContent_WhenNoError_ThenReturnsJournalArticleContentForTheArticleContent() throws Exception {
		String xmlContent = "<root><dynamic-element></dynamic-element></root>";
		when(mockJournalArticle.getContent()).thenReturn(xmlContent);

		JournalArticleContent result = journalArticleProcessorImpl.getJournalArticleContent(mockJournalArticle);

		assertThat(result.getDynamicElement().size(), equalTo(1));
	}

	private void mockJournalArticleXMLDocument(String fieldName, String expectedValue) throws DocumentException {
		when(SAXReaderUtil.read(any(Reader.class))).thenReturn(mockDocument);
		when(mockDocument.selectSingleNode("/root/dynamic-element[@name='" + fieldName + "']/dynamic-content")).thenReturn(mockNode);
		when(mockNode.getText()).thenReturn(expectedValue);
	}

	private void mockGroupLanguageId() throws PortalException {
		Locale defaultSiteLocale = Locale.ITALIAN;
		when(LanguageUtil.getLanguageId(defaultSiteLocale)).thenReturn(GROUP_LANGUAGE_ID);
		when(PortalUtil.getSiteDefaultLocale(GROUP_ID)).thenReturn(defaultSiteLocale);
	}

}
