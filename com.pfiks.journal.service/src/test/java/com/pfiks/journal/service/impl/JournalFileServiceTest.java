/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.service.impl;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.osgi.framework.FrameworkUtil;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.pfiks.journal.constants.WebContentStructure;

@PrepareForTest({ FrameworkUtil.class })
@RunWith(PowerMockRunner.class)
public class JournalFileServiceTest {

	private JournalFileService journalFileService;

	@Before
	public void setUp() {
		journalFileService = new JournalFileService();
	}

	@Test
	public void getStructureFile_WhenNoError_ThenLoadsTheStructureFile() throws Exception {
		InputStream result = journalFileService.getStructureFile(WebContentStructure.EMAIL);

		assertThat(getFileContent(result), equalTo(getTestFileContent("Email.json")));
	}

	@Test
	public void getStructureTemplate_WhenNoError_ThenLoadsTheStructureTemplate() throws Exception {
		InputStream result = journalFileService.getStructureTemplate(WebContentStructure.EMAIL);

		assertThat(getFileContent(result), equalTo(getTestFileContent("Email.ftl")));
	}

	private String getFileContent(InputStream loadFile) throws IOException {
		return IOUtils.toString(loadFile, "UTF-8");
	}

	private String getTestFileContent(String path) throws Exception {
		final URL resource = Thread.currentThread().getContextClassLoader().getResource(path);
		return getFileContent(new FileInputStream(resource.getPath()));
	}
}
