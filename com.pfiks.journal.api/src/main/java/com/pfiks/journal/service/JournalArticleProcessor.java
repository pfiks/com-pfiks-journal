/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.service;

import java.util.Map;

import com.liferay.journal.model.JournalArticle;
import com.pfiks.journal.exception.JournalProcessingException;
import com.pfiks.journal.model.JournalArticleContent;

public interface JournalArticleProcessor {

	/**
	 * Returns the content, which matches the groupId and articleId, populated
	 * with the values.
	 *
	 * @param groupId the group id
	 * @param articleId the article id
	 * @param values the new values
	 * @return the populated content
	 * @throws JournalProcessingException if any exception occurs
	 */
	public String getPopulatedContent(long groupId, String articleId, Map<String, String> values) throws JournalProcessingException;

	/**
	 * Returns the articleContent populated with the placeholder values.
	 *
	 * @param articleContent the articleContent
	 * @param placeholderValues the content placeholder values
	 * @return the populated content
	 */
	public String getPopulatedContent(String articleContent, Map<String, String> placeholderValues);

	/**
	 * Returns field name's value from the xml content
	 *
	 * @param fieldName the field name
	 * @param xmlContent the xml content
	 * @return field's value
	 * @throws JournalProcessingException if any exception occurs
	 */
	public String getFieldValue(String fieldName, String xmlContent) throws JournalProcessingException;

	/**
	 * Returns the content of the journal article in the default group
	 * languageId
	 *
	 * @param groupId the groupId
	 * @param articleId the articleId
	 * @return localized the journal article content
	 * @throws JournalProcessingException if any exception occurs
	 */
	public String getArticleContent(long groupId, String articleId) throws JournalProcessingException;

	/**
	 * Returns the content of the journal article in specified languageId
	 *
	 * @param groupId the groupId
	 * @param articleId the articleId
	 * @param languageId the languageId
	 * @return the localized journal article content
	 * @throws JournalProcessingException if any exception occurs
	 */
	public String getArticleContent(long groupId, String articleId, String languageId) throws JournalProcessingException;

	/**
	 * Retrieves the content of the journal article for all the available
	 * language ids.
	 *
	 * @param groupId the groupId
	 * @param articleId the articleId
	 * @return map containing the languageId as key and the localized article
	 *         content as value
	 * @throws JournalProcessingException if any exception occurs
	 */
	public Map<String, String> getArticleContentByLanguageId(long groupId, String articleId) throws JournalProcessingException;

	/**
	 * Retrieves the content of the journal article given for all the available
	 * language ids.
	 *
	 * @param journalArticle the journalArticle
	 * @return map containing the languageId as key and the localized content
	 *         field value
	 */
	public Map<String, String> getArticleContentByLanguageId(JournalArticle journalArticle);

	/**
	 * Retrieves the content of the journal article given for the given language
	 * id.
	 *
	 * @param journalArticle the journalArticle
	 * @param languageId the languageId
	 * @return JournalArticleContent
	 * @throws JournalProcessingException if any exception occurs
	 */
	public JournalArticleContent getJournalArticleContent(JournalArticle journalArticle, String languageId) throws JournalProcessingException;

	/**
	 * Retrieves the content of the journal article.
	 *
	 * @param journalArticle the journalArticle
	 * @return JournalArticleContent
	 * @throws JournalProcessingException if any exception occurs
	 */
	public JournalArticleContent getJournalArticleContent(JournalArticle journalArticle) throws JournalProcessingException;

}
