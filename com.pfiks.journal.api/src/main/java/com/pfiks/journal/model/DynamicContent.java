/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.pfiks.journal.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dynamic-content", propOrder = { "value" })
public class DynamicContent {

	@XmlValue
	protected String value;

	@XmlAttribute(name = "language-id")
	protected String languageId;

	@XmlAttribute(name = "alt")
	protected String alt;

	@XmlAttribute(name = "name")
	protected String name;

	@XmlAttribute(name = "title")
	protected String title;

	@XmlAttribute(name = "type")
	protected String type;

	@XmlAttribute(name = "fileEntryId")
	protected String fileEntryId;

	@XmlAttribute(name = "id")
	protected Integer id;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String value) {
		languageId = value;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String value) {
		alt = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		name = value;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String value) {
		title = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String value) {
		type = value;
	}

	public String getFileEntryId() {
		return fileEntryId;
	}

	public void setFileEntryId(String value) {
		fileEntryId = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer value) {
		id = value;
	}

}
