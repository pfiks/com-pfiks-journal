/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.pfiks.journal.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "dynamicElement" })
@XmlRootElement(name = "root")
public class JournalArticleContent {

	@XmlElement(name = "dynamic-element")
	protected List<DynamicElement> dynamicElement;

	@XmlAttribute(name = "available-locales")
	protected String availableLocales;

	@XmlAttribute(name = "default-locale")
	protected String defaultLocale;

	public List<DynamicElement> getDynamicElement() {
		if (dynamicElement == null) {
			dynamicElement = new ArrayList<>();
		}
		return dynamicElement;
	}

	public String getAvailableLocales() {
		return availableLocales;
	}

	public void setAvailableLocales(String value) {
		availableLocales = value;
	}

	public String getDefaultLocale() {
		return defaultLocale;
	}

	public void setDefaultLocale(String value) {
		defaultLocale = value;
	}

}
