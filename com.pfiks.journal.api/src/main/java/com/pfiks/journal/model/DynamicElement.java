/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dynamic-element", propOrder = { "dynamicContent", "dynamicElement" })
public class DynamicElement {

	@XmlElement(name = "dynamic-content")
	protected DynamicContent dynamicContent;

	@XmlElement(name = "dynamic-element")
	protected List<DynamicElement> dynamicElement;

	@XmlAttribute(name = "name")
	protected String name;

	@XmlAttribute(name = "instance-id")
	protected String instanceId;

	@XmlAttribute(name = "type")
	protected String type;

	@XmlAttribute(name = "index-type")
	protected String indexType;

	public DynamicContent getDynamicContent() {
		return dynamicContent;
	}

	public List<DynamicElement> getDynamicElement() {
		if (dynamicElement == null) {
			dynamicElement = new ArrayList<>();
		}
		return dynamicElement;
	}

	public String getName() {
		return name;
	}

	public void setName(String value) {
		name = value;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String value) {
		instanceId = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String value) {
		type = value;
	}

	public String getIndexType() {
		return indexType;
	}

	public void setIndexType(String value) {
		indexType = value;
	}

	public boolean isTextArea() {
		return "text_area".equalsIgnoreCase(type);
	}

	public boolean isText() {
		return "text".equalsIgnoreCase(type);
	}

	public boolean isBoolean() {
		return "boolean".equalsIgnoreCase(type);
	}

	public boolean isInteger() {
		return "ddm-integer".equalsIgnoreCase(type);
	}

	public boolean isLinkToLayout() {
		return "link_to_layout".equalsIgnoreCase(type);
	}

	public boolean isNumber() {
		return "ddm-number".equalsIgnoreCase(type);
	}

	public boolean isRadio() {
		return "radio".equalsIgnoreCase(type);
	}

	public boolean isTextBox() {
		return "text_box".equalsIgnoreCase(type);
	}

	public boolean isJournalArticle() {
		return "ddm-journal-article".equalsIgnoreCase(type);
	}

	public boolean isImage() {
		return "image".equalsIgnoreCase(type);
	}

	public boolean isSelect() {
		return "list".equalsIgnoreCase(type);
	}

	public boolean isSeparator() {
		return "selection_break".equalsIgnoreCase(type);
	}

	public boolean isDate() {
		return "ddm-date".equalsIgnoreCase(type);
	}

	public boolean isDecimal() {
		return "ddm-decimal".equalsIgnoreCase(type);
	}

	public boolean isDocument() {
		return "document_library".equalsIgnoreCase(type);
	}

	public boolean isGeoLocation() {
		return "ddm-geolocation".equalsIgnoreCase(type);
	}

}
