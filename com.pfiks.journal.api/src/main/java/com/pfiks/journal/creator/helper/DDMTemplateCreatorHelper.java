/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper;

import java.io.InputStream;
import java.util.Map;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.model.Group;
import com.pfiks.journal.exception.JournalCreationException;

public interface DDMTemplateCreatorHelper {

	public void createMissingWebContentTemplate(long companyId, long groupId, long userId, String templateFileName, InputStream inputStream) throws JournalCreationException;

	public DDMTemplate getOrCreateWebContentTemplate(Group group, DDMStructure ddmStructure, String templateName, String templateKey, InputStream inputStream) throws JournalCreationException;

	public DDMTemplate getOrCreateWebContentTemplateWithNoStructure(Group group, String templateName, String templateKey, InputStream inputStream) throws JournalCreationException;

	public void createMissingWebContentTemplates(DDMStructure ddmStructure, Map<String, InputStream> templates) throws JournalCreationException;

}
