/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper;

import java.io.InputStream;
import java.util.Map;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.model.Group;
import com.pfiks.journal.exception.JournalCreationException;

public interface JournalArticleCreatorHelper {

	/**
	 * Creates a new web content article if no other article with the same
	 * articleId exists in the group.
	 *
	 * @param group the group
	 * @param ddmStructure - optional value. If not specified the web content
	 *            will be Basic Web Content
	 * @param journalFolder - mandatory value. If nothing is set the web content
	 *            will NOT be created
	 * @param skipWorkflow set to true it will always auto-approve content, no
	 *            matter what workflow is enabled for JournalArticles
	 * @param articleFileName the article file name
	 * @param inputStream the inputStream
	 * @throws JournalCreationException if any exception occurs.
	 */
	public void createMissingJournalArticle(Group group, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleFileName, InputStream inputStream)
			throws JournalCreationException;

	/**
	 * Creates a new web content article if no other article with the same
	 * articleId exists in the group.
	 *
	 * @param companyId the companyId
	 * @param groupId the groupId
	 * @param ddmStructure - optional value. If not specified the web content
	 *            will be Basic Web Content
	 * @param journalFolder - mandatory value. If nothing is set the web content
	 *            will NOT be created
	 * @param skipWorkflow set to true it will always auto-approve content, no
	 *            matter what workflow is enabled for JournalArticles
	 * @param articleFileName the article file name
	 * @param inputStream the inputStream
	 * @throws JournalCreationException if any exception occurs.
	 */
	public void createMissingJournalArticle(long companyId, long groupId, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleFileName, InputStream inputStream)
			throws JournalCreationException;

	/**
	 * Returns the latest version of the article or creates a new web content
	 * article
	 *
	 * @param groupId the groupId
	 * @param ddmStructure - optional value. If not specified the web content
	 *            will be Basic Web Content
	 * @param journalFolder - mandatory value. If nothing is set the web content
	 *            will NOT be created
	 * @param skipWorkflow set to true it will always auto-approve content, no
	 *            matter what workflow is enabled for JournalArticles
	 * @param articleFileName the article file name
	 * @param inputStream the inputStream
	 * @return the journal article
	 * @throws JournalCreationException if any exception occurs.
	 */
	public JournalArticle getOrCreateJournalArticle(long groupId, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleFileName, InputStream inputStream)
			throws JournalCreationException;

	/**
	 * Creates a new web content article if no other article with the same
	 * articleId exists in the group.
	 *
	 * @param companyId the companyId
	 * @param groupId the groupId
	 * @param ddmStructure - optional value. If not specified the web content
	 *            will be Basic Web Content
	 * @param journalFolder - mandatory value. If nothing is set the web content
	 *            will NOT be created
	 * @param skipWorkflow set to true it will always auto-approve content, no
	 *            matter what workflow is enabled for JournalArticles
	 * @param articleId the articleId
	 * @param title the title
	 * @param content the content
	 * @throws JournalCreationException if any exception occurs.
	 */
	public void createMissingJournalArticle(long companyId, long groupId, DDMStructure ddmStructure, JournalFolder journalFolder, boolean skipWorkflow, String articleId, String title, String content)
			throws JournalCreationException;

	/**
	 * Creates new web content articles from a Map of InputStreams and are
	 * created if no other articles with the same articleId exists in the group
	 *
	 * @param companyId the companyId
	 * @param groupId the groupId
	 * @param structureKey the structureKey
	 * @param folderName the folderName
	 * @param inputStreams the inputStreams
	 * @throws JournalCreationException if any exception occurs
	 */
	public void createMissingJournalArticles(long companyId, long groupId, String structureKey, String folderName, Map<String, InputStream> inputStreams) throws JournalCreationException;

	/**
	 * Creates new BASIC web content articles from a Map of InputStreams and are
	 * created if no other articles with the same articleId exists in the group
	 *
	 * @param companyId the companyId
	 * @param groupId the groupId
	 * @param folderName the folderName
	 * @param inputStreams the inputStreams
	 * @throws JournalCreationException if any exception occurs
	 */
	public void createMissingJournalArticles(long companyId, long groupId, String folderName, Map<String, InputStream> inputStreams) throws JournalCreationException;

}