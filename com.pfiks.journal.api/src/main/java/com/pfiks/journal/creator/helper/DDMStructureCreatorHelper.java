/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper;

import java.io.InputStream;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.portal.kernel.model.Group;
import com.pfiks.journal.constants.WebContentStructure;
import com.pfiks.journal.exception.JournalCreationException;

public interface DDMStructureCreatorHelper {

	/**
	 * Creates a web content structure if no structure is found with the same
	 * structureKey
	 *
	 * @param companyId the companyId
	 * @param groupId the groupId
	 * @param userId the userId
	 * @param structureFileName the structure file name
	 * @param inputStream the inputStream
	 * @throws JournalCreationException any exception while creating the
	 *             DDMStructure
	 */
	public void createMissingWebContentStructure(long companyId, long groupId, long userId, String structureFileName, InputStream inputStream) throws JournalCreationException;

	/**
	 * Creates a web content structure if no structure is found with the same
	 * structureKey
	 *
	 * @param group the group
	 * @param structureName the structureName
	 * @param structureKey the structure key
	 * @param inputStream the inputStream
	 * @return the created DDMStructure
	 * @throws JournalCreationException any exception while creating the
	 *             DDMStructure
	 */
	public DDMStructure getOrCreateWebContentStructure(Group group, String structureName, String structureKey, InputStream inputStream) throws JournalCreationException;

	/**
	 * Creates a new web content structure in the global group if no structure
	 * is found with the same structureKey.
	 *
	 * If a structure already exists then returns the web content structure
	 * found.
	 *
	 * Also creates the linked web content templates if missing
	 *
	 * @param companyId the companyId
	 * @param structureName the structure to create
	 * @return the created DDMStructure
	 * @throws JournalCreationException any exception while creating the
	 *             DDMStructure
	 */
	public DDMStructure getOrCreateWebContentStructure(long companyId, WebContentStructure structureName) throws JournalCreationException;

}
