/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.creator.helper;

import com.liferay.journal.model.JournalFolder;
import com.pfiks.journal.exception.JournalCreationException;

@FunctionalInterface
public interface JournalFolderCreatorHelper {

	/**
	 * /** Creates a web content folder if no other journalFolder exist with the
	 * same folderName in the group. If a folder with the same name already
	 * exists, the folder found is returned
	 *
	 * @param companyId the companyId
	 * @param groupId the groupId
	 * @param folderName the folder name
	 * @param folderDescription the folder description
	 * @return the journalFolder created or the folder found
	 * @throws JournalCreationException the JournalCreationException
	 */
	public JournalFolder getOrCreateJournalFolder(long companyId, long groupId, String folderName, String folderDescription) throws JournalCreationException;

}