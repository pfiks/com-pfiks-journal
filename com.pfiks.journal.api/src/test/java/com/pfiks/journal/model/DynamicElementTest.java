/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class DynamicElementTest {

	@Test
	public void isTextArea_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isTextArea(), equalTo(false));
	}

	@Test
	public void isTextArea_WhenTypeIsNotTextArea_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isTextArea(), equalTo(false));
	}

	@Test
	public void isTextArea_WhenTypeIsTextArea_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("text_area").isTextArea(), equalTo(true));
	}

	@Test
	public void isText_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isText(), equalTo(false));
	}

	@Test
	public void isText_WhenTypeIsNotText_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isText(), equalTo(false));
	}

	@Test
	public void isText_WhenTypeIsText_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("text").isText(), equalTo(true));
	}

	@Test
	public void isInteger_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isInteger(), equalTo(false));
	}

	@Test
	public void isInteger_WhenTypeIsNotInteger_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isInteger(), equalTo(false));
	}

	@Test
	public void isInteger_WhenTypeIsInteger_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("ddm-integer").isInteger(), equalTo(true));
	}

	@Test
	public void isLinkToLayout_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isLinkToLayout(), equalTo(false));
	}

	@Test
	public void isLinkToLayout_WhenTypeIsNotLinkToLayout_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isLinkToLayout(), equalTo(false));
	}

	@Test
	public void isLinkToLayout_WhenTypeIsLinkToLayout_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("link_to_layout").isLinkToLayout(), equalTo(true));
	}

	@Test
	public void isNumber_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isNumber(), equalTo(false));
	}

	@Test
	public void isNumber_WhenTypeIsNotNumber_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isNumber(), equalTo(false));
	}

	@Test
	public void isNumber_WhenTypeIsNumber_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("ddm-number").isNumber(), equalTo(true));
	}

	@Test
	public void isRadio_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isRadio(), equalTo(false));
	}

	@Test
	public void isRadio_WhenTypeIsNotRadio_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isRadio(), equalTo(false));
	}

	@Test
	public void isRadio_WhenTypeIsRadio_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("radio").isRadio(), equalTo(true));
	}

	@Test
	public void isTextBox_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isTextBox(), equalTo(false));
	}

	@Test
	public void isTextBox_WhenTypeIsNotTextBox_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isTextBox(), equalTo(false));
	}

	@Test
	public void isTextBox_WhenTypeIsTextBox_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("text_box").isTextBox(), equalTo(true));
	}

	@Test
	public void isJournalArticle_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isJournalArticle(), equalTo(false));
	}

	@Test
	public void isJournalArticle_WhenTypeIsNotJournalArticle_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isJournalArticle(), equalTo(false));
	}

	@Test
	public void isJournalArticle_WhenTypeIsJournalArticle_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("ddm-journal-article").isJournalArticle(), equalTo(true));
	}

	@Test
	public void isImage_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isImage(), equalTo(false));
	}

	@Test
	public void isImage_WhenTypeIsNotImage_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isImage(), equalTo(false));
	}

	@Test
	public void isImage_WhenTypeIsImage_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("image").isImage(), equalTo(true));
	}

	@Test
	public void isSelect_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isSelect(), equalTo(false));
	}

	@Test
	public void isSelect_WhenTypeIsNotSelect_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isSelect(), equalTo(false));
	}

	@Test
	public void isSelect_WhenTypeIsSelect_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("list").isSelect(), equalTo(true));
	}

	@Test
	public void isSeparator_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isSeparator(), equalTo(false));
	}

	@Test
	public void isSeparator_WhenTypeIsNotSeparator_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isSeparator(), equalTo(false));
	}

	@Test
	public void isSeparator_WhenTypeIsSeparator_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("selection_break").isSeparator(), equalTo(true));
	}

	@Test
	public void isDate_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isDate(), equalTo(false));
	}

	@Test
	public void isDate_WhenTypeIsNotDate_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isDate(), equalTo(false));
	}

	@Test
	public void isDate_WhenTypeIsDate_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("ddm-date").isDate(), equalTo(true));
	}

	@Test
	public void isDecimal_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isDecimal(), equalTo(false));
	}

	@Test
	public void isDecimal_WhenTypeIsNotDecimal_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isDecimal(), equalTo(false));
	}

	@Test
	public void isDecimal_WhenTypeIsDecimal_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("ddm-decimal").isDecimal(), equalTo(true));
	}

	@Test
	public void isDocument_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isDocument(), equalTo(false));
	}

	@Test
	public void isDocument_WhenTypeIsNotDocument_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isDocument(), equalTo(false));
	}

	@Test
	public void isDocument_WhenTypeIsDocument_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("document_library").isDocument(), equalTo(true));
	}

	@Test
	public void isGeoLocation_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isGeoLocation(), equalTo(false));
	}

	@Test
	public void isGeoLocation_WhenTypeIsNotGeoLocation_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isGeoLocation(), equalTo(false));
	}

	@Test
	public void isGeoLocation_WhenTypeIsGeoLocation_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("ddm-geolocation").isGeoLocation(), equalTo(true));
	}

	@Test
	public void isBoolean_WhenTypeIsNotSet_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType(null).isBoolean(), equalTo(false));
	}

	@Test
	public void isBoolean_WhenTypeIsNotBoolean_ThenReturnsFalse() {
		assertThat(createDynamicElementWithType("unknown").isBoolean(), equalTo(false));
	}

	@Test
	public void isBoolean_WhenTypeIsBoolean_ThenReturnsTrue() {
		assertThat(createDynamicElementWithType("boolean").isBoolean(), equalTo(true));
	}

	private DynamicElement createDynamicElementWithType(String type) {
		DynamicElement dynamicElement = new DynamicElement();
		dynamicElement.setType(type);
		return dynamicElement;
	}

}
