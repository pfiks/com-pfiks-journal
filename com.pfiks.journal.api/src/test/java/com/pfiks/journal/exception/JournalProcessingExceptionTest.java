/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.exception;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class JournalProcessingExceptionTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public final void journalProcessingException_ShouldBeThrown_WithMessageAndThrowable() throws JournalProcessingException {
		thrown.expect(JournalProcessingException.class);
		thrown.expectMessage("message");
		throw new JournalProcessingException("message", new Exception());
	}

	@Test
	public final void journalProcessingException_ShouldBeThrown_WithThrowable() throws JournalProcessingException {
		thrown.expect(JournalProcessingException.class);
		throw new JournalProcessingException(new Exception());
	}

	@Test
	public final void journalProcessingException_ShouldBeThrown_WithoutMessage() throws JournalProcessingException {
		thrown.expectMessage("message");
		throw new JournalProcessingException("message");
	}

}
