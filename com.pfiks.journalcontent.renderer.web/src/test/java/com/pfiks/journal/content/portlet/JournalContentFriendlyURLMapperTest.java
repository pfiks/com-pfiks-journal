/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.content.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class JournalContentFriendlyURLMapperTest {

	private JournalContentFriendlyURLMapper journalContentFriendlyURLMapper;

	@Before
	public void setUp() {
		journalContentFriendlyURLMapper = new JournalContentFriendlyURLMapper();
	}

	@Test
	public void getMapping_WhenNoError_ThenReturnsJournalContent() {
		String result = journalContentFriendlyURLMapper.getMapping();

		assertThat(result, equalTo("journal_content"));
	}

}
