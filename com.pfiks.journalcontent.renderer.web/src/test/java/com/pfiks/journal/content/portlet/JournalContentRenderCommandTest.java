/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.content.portlet;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.journal.content.constants.ViewKeys;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ParamUtil.class, PropsUtil.class })
public class JournalContentRenderCommandTest {

	private static final long USER_ID = 456;
	private static final long GROUP_ID = 123l;
	private static final long CLASS_PK = 78;

	@InjectMocks
	private JournalContentRenderCommand journalContentRenderCommand;

	@Mock
	private AssetEntryLocalService mockAssetEntryLocalService;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private RenderRequest mockRenderRequest;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(PropsUtil.class, ParamUtil.class);
	}

	@Test(expected = PortletException.class)
	public void render_WhenException_ThenThrowsPortletException() throws Exception {
		journalContentRenderCommand.render(mockRenderRequest, mockRenderResponse);
	}

	@Test
	public void render_WhenNoError_ThenReturnsViewJSP() throws Exception {
		String ddmTemplateKey = "ddmTemplateKey";
		when(ParamUtil.getString(mockRenderRequest, "ddmTemplateKey")).thenReturn(ddmTemplateKey);
		mockBasicDetails();

		String result = journalContentRenderCommand.render(mockRenderRequest, mockRenderResponse);

		assertThat(result, equalTo(ViewKeys.VIEW_JSP));
	}

	@Test
	public void render_WhenNoError_ThenAddsJournalArticleAsRequestAttribute() throws Exception {
		String ddmTemplateKey = "ddmTemplateKey";
		when(ParamUtil.getString(mockRenderRequest, "ddmTemplateKey")).thenReturn(ddmTemplateKey);
		mockBasicDetails();

		journalContentRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("journalArticle", mockJournalArticle);
	}

	@Test
	public void render_WhenNoError_ThenIncrementsTheViewCount() throws Exception {
		String ddmTemplateKey = "ddmTemplateKey";
		when(ParamUtil.getString(mockRenderRequest, "ddmTemplateKey")).thenReturn(ddmTemplateKey);
		mockBasicDetails();

		journalContentRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockAssetEntryLocalService, times(1)).incrementViewCounter(USER_ID, JournalArticle.class.getName(), CLASS_PK);
	}

	@Test
	public void render_WhenExceptionIncrementingTheViewCounter_ThenNoErrorIsThrown() throws Exception {
		String ddmTemplateKey = "ddmTemplateKey";
		when(ParamUtil.getString(mockRenderRequest, "ddmTemplateKey")).thenReturn(ddmTemplateKey);
		mockBasicDetails();
		when(mockAssetEntryLocalService.incrementViewCounter(USER_ID, JournalArticle.class.getName(), CLASS_PK)).thenThrow(new PortalException());

		try {
			journalContentRenderCommand.render(mockRenderRequest, mockRenderResponse);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void render_WhenNoDDMTemplateKeyParamSpecified_ThenAddsJournalArticleTemplateKeyAsRequestAttribute() throws Exception {
		String ddmTemplateKey = "ddmTemplateKey";
		when(mockJournalArticle.getDDMTemplateKey()).thenReturn(ddmTemplateKey);
		mockBasicDetails();

		journalContentRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("ddmTemplateKey", ddmTemplateKey);
	}

	@Test
	public void render_WhenDDMTemplateKeyParamSpecified_ThenAddsDDMTemplateKeyAsRequestAttribute() throws Exception {
		String ddmTemplateKey = "ddmTemplateKey";
		when(ParamUtil.getString(mockRenderRequest, "ddmTemplateKey")).thenReturn(ddmTemplateKey);
		mockBasicDetails();

		journalContentRenderCommand.render(mockRenderRequest, mockRenderResponse);

		verify(mockRenderRequest, times(1)).setAttribute("ddmTemplateKey", ddmTemplateKey);
	}

	private void mockBasicDetails() throws PortalException {
		when(mockRenderRequest.getAttribute(WebKeys.THEME_DISPLAY)).thenReturn(mockThemeDisplay);
		when(mockThemeDisplay.getUserId()).thenReturn(USER_ID);
		String articleId = "articleIdValue";
		when(ParamUtil.getLong(mockRenderRequest, "groupId")).thenReturn(GROUP_ID);
		when(ParamUtil.getString(mockRenderRequest, "articleId")).thenReturn(articleId);
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, articleId)).thenReturn(mockJournalArticle);
		when(mockJournalArticle.getResourcePrimKey()).thenReturn(CLASS_PK);
	}

}
