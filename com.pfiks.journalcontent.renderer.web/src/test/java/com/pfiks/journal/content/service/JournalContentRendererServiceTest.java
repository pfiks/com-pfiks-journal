/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.content.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.journal.model.JournalArticle;

public class JournalContentRendererServiceTest extends PowerMockito {

	@InjectMocks
	private JournalContentRendererService journalContentRendererService;

	private final long GROUP_ID = 123;
	private final String ARTICLE_ID = "myArticleId";
	private final String ARTICLE_TEMPLATE_KEY = "myArticleTemplateKey";
	private final String CUSTOM_TEMPLATE_KEY = "myCustomTemplateKey";

	@Mock
	private JournalArticle mockJournalArticle;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test
	public void getFriendlyURL_WithJournalArticleParameter_ThenReturnsTheFriendlyURLForTheDefaultArticleTemplate() {
		when(mockJournalArticle.getGroupId()).thenReturn(GROUP_ID);
		when(mockJournalArticle.getArticleId()).thenReturn(ARTICLE_ID);
		when(mockJournalArticle.getDDMTemplateKey()).thenReturn(ARTICLE_TEMPLATE_KEY);

		String result = journalContentRendererService.getFriendlyURL(mockJournalArticle);

		assertThat(result, equalTo("/-/journal_content/56/" + GROUP_ID + "/" + ARTICLE_ID + "/" + ARTICLE_TEMPLATE_KEY));
	}

	@Test
	public void getFriendlyURL_WithJournalArticleAndTemplateKeyParameters_ThenReturnsTheFriendlyURLForTheSpecifiedTemplate() {
		when(mockJournalArticle.getGroupId()).thenReturn(GROUP_ID);
		when(mockJournalArticle.getArticleId()).thenReturn(ARTICLE_ID);
		when(mockJournalArticle.getDDMTemplateKey()).thenReturn(ARTICLE_TEMPLATE_KEY);

		String result = journalContentRendererService.getFriendlyURL(mockJournalArticle, CUSTOM_TEMPLATE_KEY);

		assertThat(result, equalTo("/-/journal_content/56/" + GROUP_ID + "/" + ARTICLE_ID + "/" + CUSTOM_TEMPLATE_KEY));
	}

	@Test
	public void getFriendlyURL_WithGroupIdArticleIdAndTemplateKeyParameters_ThenReturnsTheFriendlyURLForTheSpecifiedTemplate() {
		String result = journalContentRendererService.getFriendlyURL(GROUP_ID, ARTICLE_ID, CUSTOM_TEMPLATE_KEY);

		assertThat(result, equalTo("/-/journal_content/56/" + GROUP_ID + "/" + ARTICLE_ID + "/" + CUSTOM_TEMPLATE_KEY));
	}
}
