/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.content.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.pfiks.journal.content.constants.PortletKeys;

@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true", "com.liferay.portlet.display-category=category.hidden",
		"javax.portlet.display-name=" + PortletKeys.JOURNAL_CONTENT, "com.liferay.portlet.css-class-wrapper=pfiks-journal-web padded-portlet-wrapper",
		"com.liferay.portlet.header-portlet-css=/css/pfiks-journal-web.css", "javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/",
		"javax.portlet.name=" + PortletKeys.JOURNAL_CONTENT, "javax.portlet.security-role-ref=power-user,user", "javax.portlet.supports.mime-type=text/html",
		"javax.portlet.resource-bundle=content.Language" }, service = Portlet.class)
public class JournalContentPortlet extends MVCPortlet {

}
