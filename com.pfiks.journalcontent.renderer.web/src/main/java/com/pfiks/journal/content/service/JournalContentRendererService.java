/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.content.service;

import org.osgi.service.component.annotations.Component;

import com.liferay.journal.model.JournalArticle;

@Component(immediate = true, service = JournalContentRendererService.class)
public class JournalContentRendererService {

	public String getFriendlyURL(JournalArticle journalArticle) {
		return getFriendlyURL(journalArticle.getGroupId(), journalArticle.getArticleId(), journalArticle.getDDMTemplateKey());
	}

	public String getFriendlyURL(JournalArticle journalArticle, String ddmTemplateKey) {
		return getFriendlyURL(journalArticle.getGroupId(), journalArticle.getArticleId(), ddmTemplateKey);
	}

	public String getFriendlyURL(long groupId, String articleId, String ddmTemplateKey) {
		return "/-/journal_content/56/" + groupId + "/" + articleId + "/" + ddmTemplateKey;
	}
}
