/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.journal.content.portlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.pfiks.journal.content.constants.MVCCommandKeys;
import com.pfiks.journal.content.constants.PortletKeys;
import com.pfiks.journal.content.constants.ViewKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + PortletKeys.JOURNAL_CONTENT, "mvc.command.name=/", "mvc.command.name=" + MVCCommandKeys.VIEW }, service = MVCRenderCommand.class)
public class JournalContentRenderCommand implements MVCRenderCommand {

	private static final Log LOG = LogFactoryUtil.getLog(JournalContentRenderCommand.class);

	@Reference
	private AssetEntryLocalService assetEntryLocalService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		try {

			long groupId = ParamUtil.getLong(renderRequest, "groupId");
			String articleId = ParamUtil.getString(renderRequest, "articleId");
			String ddmTemplateKey = ParamUtil.getString(renderRequest, "ddmTemplateKey");

			JournalArticle journalArticle = journalArticleLocalService.getLatestArticle(groupId, articleId);

			renderRequest.setAttribute("journalArticle", journalArticle);
			renderRequest.setAttribute("ddmTemplateKey", Validator.isNotNull(ddmTemplateKey) ? ddmTemplateKey : journalArticle.getDDMTemplateKey());

			incrementCounter(renderRequest, journalArticle);

			return ViewKeys.VIEW_JSP;
		} catch (Exception e) {
			throw new PortletException(e);
		}
	}

	private void incrementCounter(RenderRequest renderRequest, JournalArticle journalArticle) {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			assetEntryLocalService.incrementViewCounter(themeDisplay.getUserId(), JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
		} catch (Exception e) {
			LOG.debug(e);
			LOG.error("Unable to increment view counter - " + e.getMessage());
		}
	}

}
