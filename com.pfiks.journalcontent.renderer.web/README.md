# Journal Content Renderer Web
The module displays a web content article using the following friendly URL mappings

### Display the article using the article default template
${FULL_PAGE_URL}/-/journal_content/56/${groupId}/${articleId}

### Display the article using the specified template
${FULL_PAGE_URL}/-/journal_content/56/${groupId}/${articleId}/${ddmTemplateKey}

## Configuration
The portlet must be added to the whitelist.
Edit portal-ext.properties and add 'com_pfiks_journal_portlet_JournalContentPortlet' to property 'portlet.add.default.resource.check.whitelist'
